+++
title = "Entresse Kirjasto"
date = 2023-07-22
draft = false
[taxonomies]
tags = ["finland"]
+++

Я вскользь упоминал, что библиотеки тут крутые.
Настало время раскрыть тему!

<!-- more -->

С приходом интернета библиотеки, в большинстве своём, стали почти не актуальны.
Помимо школьной и университетской, я заходил в одну рядом с домом в Москве году в 2018.
Я тогда читал книжку Стивена Хокинга и хотел иметь физический экземпляр, но библиотекарь про этого автора даже не слышала 🤷
Разочаровавшись, я туда больше не ходил. Делать там как-то было особо нечего.
Я уверен, что в России есть очень крутые библиотеки, но я просто в них не был.

Про финские kirjastot я был наслышан, особенно про центральную -- Oodi.
Мы туда заходили 2 раза, и да, это и парк, и музей, и коворкинг, и всё бесплатно.
Но мне кажется показательней наша нынешняя библиотека рядом с домом.

Мы живём в Espoo, это город-спутник Хельсинки, типа как Красногорск для Москвы, только масштаб в 10 раз уменьшить.
"Исторический центр города" тут весьма уныл, основная часть была возведена в 70-е, и с тех пор не сказать что реновировалась, так что выглядит довольно потаскано.

{{ img(src="espoontori.png", comment="Задний вход в Espoontori -- торговый центр напротив библиотеки") }}

Вся жизнь проходит на южной стороне центрального парка: там очень современный город, красивые дома, метро, мои оба офиса.
А тут остался транспортный хаб и социальное жильё (поэтому думаем вскоре отсюда перебираться).
Это я всё к тому, что у нас далеко не элитный район с богатыми налогоплательщиками, а как раз наоборот.

Сама библиотека находится на верхнем этаже торгового центра, стоящего напротив того, что на фотке выше. Называется Entresse, и выглядит получше:

{{ img(src="entresse.png") }}

## Книги

Внутри, конечно, есть книги, много книг.
Чтобы взять книжку, нужен читательский билет, который выглядит как обычная карточка.

{{ img(src="IMG_20230719_142551.jpg", comment="На выходе из библиотеки стоят такие терминалы. Там сначала сканируешь свою карточку, потом всё, что взял, и можешь идти домой.") }}

Книги, как правило, берутся на месяц. За неделю на электронную почту приходит письмо с напоминанием вернуть их. Если на книгу нет очереди, то её можно продлить онлайн ещё на месяц, вплоть до полугода.

{{ img(src="IMG_20230719_142652.jpg", comment="Чтобы книгу сдать, её нужно положить на такой конвейер") }}
{{ img(src="IMG_20230719_141702.jpg", comment="Мы постоянно берём детскую литературу, её выбор на русском довольно большой. Очередей на неё нет, поэтому если Аири нравится, то оставляем надолго") }}

Если нужной книги нет, её можно заказать. Иногда бюджет библиотеки заканчивается, тогда они ограничивают покупки произведений на иностранных языках этим и прошлым годом выпуска. Мне так в заказе на `Designing Data-Intensive Applications` отказали, но вскоре она всё равно появилась, но на неё теперь очередь 😓

{{ img(src="berserk.png", comment='На очень популярную мангу "Berserk" очередь аж 40 человек') }}

В целом, они стараются иметь всё актуальное, что пользуется спросом.

## ... и не только

Но всё это не слишком примечательно, ведь помимо книг есть ещё гора сервисов, которые и делают библиотеку особенной.

{{ img(src="IMG_20230719_141714.jpg, IMG_20230719_141857.jpg", comment="Тут и там стоят телевизоры с игровыми приставками, компы и места для работы. Дети играют, взрослые работают.") }}

Я видел Nintendo Switch, Playstation 5, XBox не знаю какой. Рядом с ними в аренду можно взять фильмы на DVD -- забавный контраст :)

{{ img(src="IMG_20230719_141718.jpg, IMG_20230719_140644.jpg", comment="Есть игровая зона для детей. Там небольшая детская кухонька, игрушки, стол с пластилином, красками и карандашами, много мягких игрушек и гигантские счёты. Рядом стойки с детской литературой.") }}
{{ img(src="IMG_20230719_142001.jpg", comment="Кстати о кухне, есть ещё одна для взрослых") }}
{{ img(src="IMG_20230703_115327.jpg", comment="Есть Моргот") }}
{{ img(src="IMG_20230719_141634.jpg, IMG_20230719_141803.jpg", comment="... какая-то аудиотехника, швейная машинка, паяльник...") }}
{{ img(src="IMG_20230719_141820.jpg", comment="... машинка для печати на футболках...") }}
{{ img(src="IMG_20230719_141834.jpg", comment="... 3D-принтеры...") }}
{{ img(src="IMG_20230719_141932.jpg", comment="... столы для пинг-понга, бильярд...") }}
{{ img(src="IMG_20230719_142612.jpg, IMG_20230719_142621.jpg", comment="... и куча настолок!") }}

Последние, правда, можно брать только на 2 недели.

## Outro

В общем, библиотеки сумели трансформироваться под современные реалии и, помимо проката книг, стали местом, где можно проводить время, учиться, развлекаться и творить. Мы туда наведываемся почти как в парк развлечений, часто берём домой книжки и настолки. Я вынашиваю планы что-то напечатать на 3D-принтере 😄
