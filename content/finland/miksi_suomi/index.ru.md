+++
title = "Miksi Suomi?"
date = 2021-04-23
toc = true
draft = true
[taxonomies]
tags = ["thoughts", "longread"]
+++

Меня ~~часто~~ иногда спрашивают, почему именно Финляндия?
Есть же Италия / Сочи / Крым / Опалиха ? С "зарплатой IT-шника" там же отлично жить!

Сейчас я постараюсь как могу собрать свои размышления на этот счёт.
Естественно, всё крайне субъективно, и ваше мнение может не совпадать с моим — это нормально 🙂

<!--more-->

## Setup

Не секрет, что зарплаты у нас, скажем так, выше средних.
Ситуация с коронавирусом вынудила компании перевести сотрудников на удалёнку и многие открыли для себя, что так полне нормально существовать.
В итоге, многие работодатели не собираются принуждать к возвращению в офис, в работники в целом не очень-то и хотят.
Короче, удалёнка из экзотики превращается в норму.

Это довольно сильно меняет рынок IT-труда.
Теперь гораздо более реально можно сидеть в глухой тайге
и работать на московскую компанию, получая 100К+.
Раньше такие вакансии нужно было хорошо поискать, и платили меньше, чем офисным.
Можно пойти ещё дальше и устроиться в зарубежную компанию, получая раза в 3 больше, даже с учётом налогов.

При этом напоминаю, что средняя зарплата в России 35К.
Те, кто смещает это среднее вверх, находятся преимущественно в Москве и Питере, поэтому среднее по тайге будет ещё меньше.
Естественно, это влияет на стоимость жилья, продуктов и услуг, так что тут действительно можно шиковать и жить как хочется.
В чём подвох?

## The problem

Представим, что у вас очень много денег и вы решили бросить кости в Сочи (привет, Саша 👋).
Вы покупаете квартиру в элитном районе, ваши дети ходят в частную школу, а сами вы посещаете частного доктора.
Жизнь прекрасна, все проблемы решены.
Чтобы вокруг не творилось, вы находитесь в замечательной закрытой экосистеме, полностью изолированной от страшных муниципальных учреждений.


