+++
title = "QBasic Gorillas"
date = 2023-11-12
draft = true
[taxonomies]
tags = ["dos"]
+++

{{ dos(name="GORILL10") }}

QBasic Gorillas (1991, Microsoft) -- одна из первых игр, с которой знакомились пользователи DOS.
Она поставлялась вместе с системой, и служила для демонстрации возможностей языка QBasic.

Для меня это была _самая_ первая игра :)
Эти гориллы и улыбающееся солнце -- первое, что я увидел на компьютере, когда пришёл однажды домой в далёком 95м.


Геймплей довольно... ограниченный. Два игрока по очереди вводят значения направления и силы броска банана, пытаясь попасть в гориллу противника.
Стрелка снизу показывает направление ветра.

В целом, игры в этом разделе будут становится интереснее и интереснее :)
