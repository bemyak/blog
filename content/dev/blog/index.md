+++
title = "How this blog is made"
date = 2021-08-17
updated = 2024-10-21
draft = false
[taxonomies]
tags = ["blog", "how-to"]
+++

A brief overview of how this blog works under the hood.

<!-- more -->

> **TL;DR**:
>
> - [Zola](https://www.getzola.org/) — static site generator
> - [Kroki](https://kroki.io) — diagram rendering
> - [Bulma](https://bulma.io/) — CSS framework
> - [Cloudflare Pages](https://developers.cloudflare.com/pages/) — hosting
> - [Namecheap](https://namecheap.com) — domain name
> - A few hacks — as always
> - No JavaScript :)

## Prehistory / Why?

It all started when I was writing a [travel blog](https://t.me/me_andshe) (in Russian).
After some time, it evolved from just a stream of photos to more deep and thoughtful content, which required some thought put in it as well as editing, planning, drafts and formatting.
The natural way to keep these drafts was in Markdown, and generating a static website was in a stone's throw from here.

Also, I was thinking about a place to dump my developer's notes.
All in all, I started looking into static site generators and decided to go with...

## [Zola](https://getzola.org/)

Unlike {{ hint(text="Hugo", hint="the most popular static site generator out there") }}, Zola is much younger, and thus its community is much smaller.
There are no myriads of fancy pre-made themes that look awesome.
But for me, this turned out to be a perfect match.

I started with Hugo, but quickly got overwhelmed by its complexity.
I picked up a good-looking theme and tried to hack on it, but OMFG that was tough! Lots of JavaScript everywhere, weird templates, TONS of CSS...
It was unbearable to change anything: things just kept falling apart.
At this point, I realized that the only possibility to make things look the way I wanted them was to create a theme from scratch.

Looking around, I found Zola and decided to try it (yeah, because of Rust).
And it was so much more sensible! I started hacking on one of the default themes but quickly rewrote it all from scratch.

The basic idea is simple:

- You write your posts in Markdown (e.g. this page looks [like this](https://gitlab.com/bemyak/blog/-/blob/master/content/dev/blog/index.md))
- You have templates that explain Zola how to build a web page from it:
  - `index.html` - the front page. I just symlinked it to `section.html`, since they are the same
  - `section.html` - a template for a page with post list
  - `page.html` - a template for a page with a post itself
- The template language ([Terra](https://tera.netlify.app/docs/)) is very similar to Jinja2, which should be familiar to all Ansible users
- Templates can _extend_ other pages, e.g. I use a `base.html` template that contains header and other common stuff
- Sass is supported out-of-the-box!
- To extend Markdown syntax, you can use _shortcodes_. Shortcode is also a template that can be inserted from Markdown, e.g. `templates/shortcodes/hint.html`:

  ```html
  <span
    class="has-tooltip-arrow {% if hint | length > 20 %} has-tooltip-multiline {% endif %}"
    data-tooltip="{{ hint }}">
    {{ text }}
  </span>
  ```

  will allow you to create tooltips {{ hint(text="like this", hint="hint text goes here") }}:

  ```markdown
  {{/* hint(text="like this", hint="hint text goes here") */}}
  ```

  How awesome is that? I use shortcodes to insert:

  - Google Maps iframes
  - Open Street Maps iframes
  - YouTube iframes
  - Images and videos with nice-looking comments
  - Katex formulas

## [Kroki](https://kroki.io)

I like keeping diagrams in plain text format.
It makes them easy to edit, read and share.
It also improves collaboration, since you can now make readable Pull/Merge Requests.

A good tool for such task is [PlantUML](https://plantuml.com/).
It even provides an [API](https://plantuml.com/server) to render images.
However, you need to send your diagram in GET request, using [special encoding](https://plantuml.com/text-encoding), which Zola doesn't support.
POST requests are [implemented](https://github.com/plantuml/plantuml-server/pull/74), but not enabled on the public server.

Luckily, there is Kroki! It wraps a lot of tools under a unified API: you just send POST requests with your diagram data and get SVG or PNG back.
We only need to write a shortcode:

```html
{% set postdata = load_data(
    url="https://kroki.io/plantuml/svg",
    format="plain", method="POST",
    content_type="text/plain",
    body=body,
    required=true
	)
%}
<figure class="image">
  {{ postdata | safe }} {% if comment %}
  <figcaption class="comment">
    <em>{{ comment | markdown | safe }}</em>
  </figcaption>
  {% endif %}
</figure>
```

So now this section in Markdown

```plantuml
{%/* plantuml(comment="simple diagram") */%}

participant Zola
participant Kroki

Zola -> Kroki: POST /plantuml/svg\nContent-type: text/plain\n<diagram_data>
activate Zola
Kroki -> Zola: SVG image
deactivate Zola
Zola -> Zola: embed SVG into HTML

{%/* end */%}
```

… renders into this:

{% plantuml(comment="simple diagram") %}
participant Zola
participant Kroki

Zola -> Kroki: POST /plantuml/svg\nContent-type: text/plain\n<diagram_data>
activate Zola
Kroki -> Zola: SVG image
deactivate Zola
Zola -> Zola: embed SVG into HTML
{% end %}

The rendering happens only once - during the site generation step.
A diagram then exists as SVG image embedded right into HTML.
A cool feature with such approach is that users can copy text from diagram labels, but a downside is that you can't just download an image.
However, you can extract SVG code from the DOM and put it into a file.

## [Bulma](https://bulma.io)

It's hard to create a good-looking site with plain HTML nowadays.
I use Bulma as my CSS framework, and I am mostly happy with it:

- No JavaScript required
- Modularity, meaning smaller asset size
- Sane configuration

Downsides:

- Has some rough edges, when you need to [hack some styles](https://gitlab.com/bemyak/blog/-/blob/master/sass/site.scss#L61) with custom overrides. Not many, but still.
- Isn't developed very actively. But maybe that's an advantage in the modern world, who knows? :)

The whole Bulma repo is added as a git submodule to the blog.
Then from `sass/site.scss` I can import the required components, and Zola will compile them into a single CSS file.

The result is pretty awesome: I maintain only ~250 LOC SCSS stylesheet, most of which is a custom {{ hint(text="table of content implementation", hint="notice how it scrolls with the page and adapts to different screen size!") }}.
This file is rendered into a ~200KB CSS file, which is a bit too much, but not drastically.

## [GitLab](https://gitlab.com)

I use Gitlab as a hosting service for the code, because it's the only non-GitHub option available in Cloudflare Pages.
The [CI setup](https://gitlab.com/bemyak/blog/blob/master/.gitlab-ci.yml) is pretty straightforward:

1. Install Zola
1. Render the blog
1. Copy it to the `/public` folder

The artifacts are not actually used, since Cloudflare Pages run their own pipelines on every push.
But it's nice to have a historical backups, I guess.

## [Cloudflare Pages](https://developers.cloudflare.com/pages/)

CloudFlare is nice, because it's EVERYWHERE, which means the best experience for the end user.
It also supports Zola, though it's not documented anymore for some reason :slightly_smiling_face:

Also it gives _some_ analytics, even though it's very highlevel (for the best).

{{ img(src="cloudflare_analytics.png") }}

## [Namecheap](https://namecheap.com)

As the name suggests, it's a cheap domain registrar.
That's it.

I decided to go with d `.net` domain, because I liked the idea of having a _network_ of my own.
There are just 2 services on it, but I plan to expand it in the future.

## Hacks

One last problem is that by the web standards, any website should have a `www.` resource, but GitLab doesn't provide it.
The solution is to host a web server somewhere that will do just one thing: redirect `www.your-blog.com` to `your-blog.com`.
Not having this resolution will make your site even less favorable by search engines, so it's a good idea to have it.

Once you have a web server, you could add a `www` A-record to a DNS service that will point to that web server.
But where to find it?

Luckily, I was already running a server for my Telegram Bot (the post about it is coming soon!), so all I had to do is to change the Caddy's configuration file a bit to add the redirect.

The server runs on Oracle Cloud, which gives you two VPSes for free.
Take a look at my [terraform scripts](https://gitlab.com/bemyak/infrastructure), which create these resources.

## JavaScript :no_good:

You might have noticed that I try to avoid JavaScript as hard as I can.
JS is a cool language with lots of great features, but using it on a simple static website seems like an overkill to me.
I advocate for using it when it really makes sense: dynamic applications with intensive user interactions.
My blog is definitely not in that category :slightly_smiling_face:

Unfortunately, nowadays, JS sticks out of everywhere, even CSS frameworks tend to depend on it, which is bizarre in my opinion.
You still need to have JS enabled to view the blog properly, since iframes use it a lot (e.g. Google Maps, YouTube), but it is not required for the basic functionality.

This also means that I have no trackers and I don't know how popular this blog is, which posts are interesting and which need improvement.
However, I consider this a good thing.
I (imagine that I) write content mostly for myself and a few people that I'm familiar with.
This takes a lot of pressure off, so I can relax and not think about views and being 100% accurate all the time.

## Conclusion

Overall, I'm pretty happy with the setup right now.
The blog is lightweight enough to my taste, easy to maintain and hack on.
It also costs absolutely nothing.

The editing process is enjoyable: markdown allows me to focus on writing and not spend too much time on technicalities.
Once the post is pushed and `draft` is set to `false` in a preface, the job is done.

## Services used in the past (left for historical reasons)

### ~~[Freenom](https://freenom.com)~~

Freenom is a (free) domain registrar, which serves `.tk`, `.ml`, `.ga`, `.cf` and `.gq` zones.
For free, you can get a domain only for a year, and then you are required not forget to renew it.
The other downside is that there are plenty of scam sites in these zones, so search engines **do not even try** to index anything there.

> You won't find my blog on Google because of that :)

The other problem is that it is not quite stable: even the front page doesn't respond sometimes.
Also, one of my domains got deregistered for no reason, and I can't get it back as it entered some weird state and disappeared from their system completely a few years ago :shrug:

If you are willing to live with these handicaps, welcome!

**Update**: The `ml` domain was taken back from FreeNom by the government of Mali.
I should have been able to still renew it, but FreeNom didn't allow me to do so for unknown reasons.
Welp, I'm on `.net` now.
