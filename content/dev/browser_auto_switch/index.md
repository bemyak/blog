+++
title = "Automatically switching the default browser"
date = 2022-05-07
updated = 2022-07-25
draft = false
[taxonomies]
tags = ["how-to", "linux"]
+++

I use two browsers in my daily life: Chromium for work-related stuff and Firefox for everything else. This helps me to keep a strict separation between these two worlds, including: logins, bookmarks, history, settings (e.g. proxy, synchronization), etc. Let's see how to make a default browser switch automatically to the correct one.

<!-- more -->

> **Note**: You could probably do the same using profiles, but for me, it is much easier to just have two completely independent browsers

## CLI magic

To set the default browser (Firefox in this case) on a user-level, you need to execute two commands:

```bash
xdg-settings set default-web-browser firefox.desktop
xdg-mime default firefox.desktop x-scheme-handler/http x-scheme-handler/https
```

The first one tells your Desktop Environment to use the new default. However, to my testing, this is not always enough. We need to update `~/.config/mimeapps.list` file as well and that is what the second command is for.

## Automation with systemd

This is all nice and simple, but our goal is to stop thinking when to do the switch, so we should automate this. Cron is not a good choice for this: our computer might be down. There is also [at](https://salsa.debian.org/debian/at) utility, but I am unsure if it will execute commands after boot if the time is missed. We will rely on systemd for this.

First, let's create one-shot "services" with our commands. We can do this on a user-level:

{% code(lng="ini", file="~/.config/systemd/user/set-browser-firefox.service") %}
[Unit]
Description=Set Firefox as the default browser

[Service]
ExecStart=/usr/bin/xdg-settings set default-web-browser firefox.desktop
ExecStart=/usr/bin/xdg-mime default firefox.desktop x-scheme-handler/http x-scheme-handler/https
Type=oneshot
{% end %}

{% code(lng="ini", file="~/.config/systemd/user/set-browser-chromium.service") %}
[Unit]
Description=Set Chromium as the default browser

[Service]
ExecStart=/usr/bin/xdg-settings set default-web-browser chromium.desktop
ExecStart=/usr/bin/xdg-mime default chromium.desktop x-scheme-handler/http x-scheme-handler/https
Type=oneshot
{% end %}

Now we need to create corresponding timers:

{% code(lng="ini", file="~/.config/systemd/user/set-browser-firefox.timer") %}
[Unit]
Description=Set Firefox as default browser in the evenings

[Timer]
OnCalendar=_-_-\* 18:00:00
Persistent=true

[Install]
WantedBy=timers.target
{% end %}

{% code(lng="ini", file="~/.config/systemd/user/set-browser-chromium.timer") %}
[Unit]
Description=Set Chromium as default browser on workdays

[Timer]
OnCalendar=Mon-Fri _-_-\* 06:00:00
Persistent=true

[Install]
WantedBy=timers.target
{% end %}

All we need to do now is to enable and start these timers:

```bash
systemctl --user daemon-reload
systemctl --user enable set-browser-firefox.timer
systemctl --user start set-browser-firefox.timer
systemctl --user enable set-browser-chromium.timer
systemctl --user start set-browser-chromium.timer
```

We can check if they are scheduled correctly:

```bash
$ systemctl --user list-timers
NEXT                         LEFT           LAST PASSED UNIT                       ACTIVATES
Sat 2022-05-07 18:00:00 EEST 3h 9min left   n/a  n/a    set-browser-firefox.timer  set-browser-firefox.service
Mon 2022-05-09 06:00:00 EEST 1 day 15h left n/a  n/a    set-browser-chromium.timer set-browser-chromium.service
```

> **Hint** When going on vacation you can just disable one of the timers: `systemctl disable --user set-browser-chromium.timer`

Done!
