+++
title = "Am I Secure?"
slug = "security"
date = 2024-10-22
[taxonomies]
tags = ["how-to"]
+++

I often see how people make security decisions based on pure intuition.
Can I store TOTP in my password manager?
Should I use a local password manager or is a remote one OK?
Is it OK to configure multiple second factors?

Since I have a degree in Information Security 🤓, I think I'll try to clarify these questions by describing the underlying theory, so you'll have a _decision framework_ to make educated choices.

<!-- more -->

## The Two Pillars of Security

The purpose of authentication is simple: verify that the user is the one who they are claiming to be.
So, how do we do this?
In general, there are two ways:

- Check that the user **knows** some secret (password, key phrase, pin code)
- Check that the user **has** something unique (key card, specific device, physical features)

These two categories are fundamentally different.
Knowledge cannot be easily transferred without the user knowing it, but the user can share it on purpose without organization's acknowledgement.
Possessions cannot be accessed remotely and are designed not to be copied easily.

That's why requiring both is a very good idea.
It's called two-factor authentication or 2FA.

Note that requiring two passwords is **NOT** 2FA, at the "factor" is the same for both passwords: knowledge.
If one password is leaked, the chances are the second one was leaked together with it through the same channel.
That's why no sane security system would ask you for two passwords.

## Convertibles

An interesting property of these factors is that they can be converted between one another.

A password can be written down on a piece paper, thus becoming a possession factor.
The paper can then be put into a safe, thus becoming a 2FA secret: you need to know the lock code AND have physical access to the safe.

Or a TOTP key can be put in a password manager.
If the password manager is protected only by a password, the secret becomes knowledge-based.

One other dangerous pattern that I saw (and used, I'm no saint here) is the overuse of biometric authentication on your mobile phone.
If you use a fingerprint to unlock your phone and then the fingerprint to unlock your password manager, welp, all your secrets were effectively converted to 1FA.

## Local password managers

So, how do password managers fit into this?
Let's consider KeePassXC or any other local password manager.

If you put a password behind another password or pass phrase, it remains a knowledge-based secret.
However, reading it also requires access to the encrypted file.
If the file is stored only on your local disk, it's 2FA, congratulations!

> Note, that the end system protection remains 1FA of course, to crack it you only need to brute-force one password.
> But at least on your system that information is stored safely.

The security improvement comes from the fact that you don't have to remember the password from 3rd party services anymore, you can generate them extremely long and store safely
behind 2FA as described above.
However, there's no way a password manager can turn 1FA into 2FA.

## Password manager as a service

If you're using a remote password manager (Bitwarden, LastPass, 1Password, etc), the situation is slightly different.
By default, they will ask you to set only the master password.
And since the vault can be accessed from anywhere, it turns all your security into 1FA.

Follow me: you have for example a Proton Mail account (which I highly recommend that you do).
You've configured 2FA there, but you've put both your password and TOTP into Bitwarden.
If Bitwarden is protected only by the master password (no TOTP), it makes your Proton account secured only with one factor.

OK, what if you have 2FA configured for the password manager?
Well, let's break this down:

- If your Proton account is secured only with one factor, it doesn't improve anything
  (apart from being able to generate a really complex password as mentioned in the previous section)
- If it has 2FA, then it remains 2FA

However, in any case the _attack surface_ is bigger now: a hacker can either hack your password manager or your Proton account.
And password manager becomes a prime target for attackers, as it gives access to everything you have online.
On one hand, a good password manager service is audited and _should_ be secure.
On the other hand, again, the attack surface is ENORMOUS:

- Backend services
- Their internal database
- All their infra
- CI/CD pipelines
- Browser extensions
- Extension store
- All the upstream libraries (a.k.a supply chain)
- Communication between all of the above

Should you stop using password managers? No, absolutely not!
But, in my opinion, storing TOTPs there is a malicious functionality, and should not be used for anything critical.
Putting all your identities in one place is very convenient, but the stakes are too high.
Divide and conquer!

## Recovering

There's always a compromise between security and usability.
But there's a third factor, which is often overlooked until it's too late: recoverability.

Imagine you're in a different country for a business trip and your luggage got lost together with your laptop, and you have only your phone.
Will you still be able to access your data?
What if the phone is stolen too?
What if you have amnesia and you've forgotten all your passwords?

> Do you sense how it goes into the opposite direction of "always have two factors, period"?

It's up for you to decide from which scenarios you want to protect yourself, but as the absolute minimum your security system should survive the loss of at least one device.

## My setup

Since I don't believe in security by obscurity, here's my setup:

I have Nextcloud. I consider it a secure storage.
It is protected with 2FA:

- The password is very complex, stored in a KeePassXC
- TOTP is stored on my phone in the Aegis app, unlocked by biometry

Both KeePassXC and Aegis is backed up (with encryption) in the same Nextcloud, thus creating a secure loop: to access Nextcloud you need access to Nextcloud.

Nextcloud data is synced between multiple devices, so loosing one of them:

- doesn't lock me out of the system: I still have access to other devices
- doesn't compromise my security: My devices and backups are encrypted with different keys

So, to my analysis, I'm not downgrading 2FA to 1FA at any point, while having ways to recover and maintaining fairly minimal attack surface.

## KeePassXC

In the light of the raised concerns with Bitwarden, I would like to take a moment to advertise KeePassXC as a great alternative.
I was avoiding it for a long time, since having "local-only" password manager seemed like a thing from the past.
However, it turned out to be GREAT!

It can integrate with any browser, much better than Bitwarden.
The latter is implemented as a browser extension, so it keeps all you data in the browser somewhere.
The KeePass extension, on the contrary, just talks to the main app, so the extension is very small.

It has ssh-agent integration, and also can act as a `org.freedesktop.secrets` provider on Linux, keeping all your secrets in one place.

The mobile app (KeePassDX) is amazing, much more robust compared to Bitwarden.

The only thing "missing" (by design) from the end user perspective is syncing, but you can use Nextcloud or Syncthing.

I highly encourage you to give it a fair try, as it has imports from all popular export formats of other password managers.

Don't give your passwords to anyone! :)

## Takeaways

- Always use 2FA
- Distinguish secrets you **know** and secrets you **have**
- Make sure you don't accidentally convert one into another
- Remember that your security is as good as the weakest part in it
- Don't store your 2nd factors in a password manager
- Think about how to recover from device or memory losses
