+++
title = "How to make money on Open Source"
date = 2021-10-06
draft = false
[taxonomies]
tags = ["thoughts"]
+++

I heard a lot of discussion around the "How to make money on opensource?" topic.
This is something I don't get completely, as at seems obvious, so I'll dump my thoughts here.

<!-- more -->

## For individuals

If you're an individual contributor, you have two choices, that are pretty similar:

1. Get hired in a company that contributes to opensource. A lot of enterprises are fine with publishing their tooling, non-essential components, libraries, etc. ATM I work for a small department that is happy to open anything, we just don't have enough time :sweat_smile: We do our best though
2. Get hired in a non-profit organization or foundation that maintains some product. The way in usually includes joining a community, contributing your spare time, moving closer to the core team and hoping for the best. There are some great examples in Blender, Godot, Rust foundations.

> Voilà, you're doing making money **writing** opensource! :tada:

## For companies

If you're a company, then making money on opensource is even easier: just use it to build your product.
You're probably already doing this by taking use of Linux, Apache, Nginx, Kafka, Kubernetes, etc, etc, etc.
So, opensource helps you to make money.
Please contribute back if possible, make donations, share your patches.

> Voilà, you're doing making money **using** opensource! :tada:

If you're a company AND you want to make money by doing opensource, please don't.
There are some examples of more or less successful businesses doing it (RedHat, Confluent, Mozilla, Gitlab), but in general this is a harmful path of pain, that leads nowhere.

Providing _consulting services_ you will naturally be inclined to do a messy configuration, obscure behavior and poor documentation.

An alternative "Open Core" model seems better, until you reach some limit of growth.
After that point the only option will be moving features behind the paywall, which will scare off even the most loyal customers (see the [recent Docker actions regarding Docker Desktop](https://www.docker.com/blog/updating-product-subscriptions))

## Foundations

The only way for an open product to bloom is to have a **foundation** behind it.
Various entities and actors need common problems to be solved, so even mutual competitors will give their money to get rid of a common headache.
The great example is Blender.

Moviemakers, game developers, animators, programmers, architects, enthusiasts - all need a proper tool to make 3D models.
This tool needs to evolve fast, be pluggable, scriptable, modular, flexible.
Adobe failed to satisfy these requirements, and luckily we have Blender now.
It is funded from the [Blender Foundation](https://www.blender.org/about/foundation/), which receives money from anybody and hires developers, managers, CTO, CEO, and other stuff.
All of these people are "making money" on Blender.
Since Blender is not a _commercial_ company, clients are fine with investing money in it to support one theirs business' pillar.

The same model successfully works for Linux, Godot, Rust and many others.
And to me, it is proved to be a thriving model.
Yep, nobody will get as rich as Jeff Bezos, but we will have an honest piece of software which is created with the shared interests taken into account.
