+++
sort_by = "update_date"
title = "Software Development"
insert_anchor_links = "left"
+++

Here I put some notes, cool discoveries, and other technical stuff.
Just like the rest of the blog, it is mostly for me, but if you'll find this interesting, I'd be super happy!
