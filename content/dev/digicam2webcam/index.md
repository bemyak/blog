+++
title = "Using digicam as a webcam on Linux"
date = 2021-11-09
updated = 2023-05-06
draft = false
[taxonomies]
tags = ["linux", "how-to"]
+++

Buying a proper webcamera can turn out pretty costly, so why not fetch your old camera from a cellar and plug it into your laptop?
How hard can it be?

<!-- more -->

I recently dug out a pretty old [Canon EOS 550D](https://en.wikipedia.org/wiki/Canon_EOS_550D), and since I only have a poor built-in webcam, that was a chance to improve my video quality significantly!

Once plugged into a USB port, the camera is detected just as storage.
We need two things to make it work properly:

1. Stream the video feed from it
1. Pipe this feed into a virtual video input device

## Grabbing the video feed

The first step is pretty simple — just use `gphoto2`:

```bash
$ # Check if the camera is detected correctly
$ gphoto2 --abilities
Abilities for camera             : Canon EOS 550D
Serial port support              : no
USB support                      : yes
Capture choices                  :
                                 : Image
                                 : Preview
                                 : Trigger Capture
Configuration support            : yes
Delete selected files on camera  : yes
Delete all files on camera       : no
File preview (thumbnail) support : yes
File upload support              : yes
$ # Now get the video!
$ gphoto2 --stdout --capture-movie
Capturing preview frames as movie to 'stdout'. Press Ctrl-C to abort.
<enjoy the matrix>
```

## Creating a fake video device

The second piece of the puzzle is `v4l`, in particular, `v4l2loopback`.
If you're using Arch like me, you can just install it:

```bash
pacman -S v4l2loopback-utils
```

This will also install the required DKMS package to bring in the kernel module named `v4l2loopback`.

Now run

```bash
sudo modprobe v4l2loopback
```

To create a virtual video device.
You can check the kernel logs for the exact path; in my case, it was `/dev/video2`

Now we can stream the video feed from the camera to that device using `ffmpeg`!

```bash
gphoto2 --stdout --capture-movie | \
ffmpeg -i - -vcodec rawvideo -pix_fmt yuv420p -threads 0 -f v4l2 /dev/video2
```

> **NB**: If this command fails with a permission error, make sure that your user is in `video` group:
> `sudo usermod -aG video $USER`

Now we need to enable this module on boot:

{% code(lng="conf", file="/etc/modules-load.d/v4l2llopback.conf") %}
v4l2loopback
{% end %}

And finally let's set default module options:

{% code(lng="conf", file="/etc/modprobe.d/v4l2loopback.conf") %}
options v4l2loopback exclusive_caps=1 card_label="GPhoto2 Webcam"
{% end %}

> **NB**: After reboot, this module will load very early, earlier that other camera drivers, so the device path will be `/dev/video0`

Reboot and verify that this command still works:

```bash
gphoto2 --stdout --capture-movie | \
ffmpeg -i - -vcodec rawvideo -pix_fmt yuv420p -threads 0 -f v4l2 /dev/video0
```

## Automation

Manually running a script every time we join a video call is not fun.
Luckily, we have two ways of solving this, pick one!

### Full

Let's automate staring the capture using `udev`.

First, create a _udev script_

{% code(lng="bash", file="/lib/udev/gphoto2-start-capture") %}
#!/bin/bash

# Uncomment the next line in you want the camera to focus before capturing

#gphoto2 --set-config autofocusdrive=1
gphoto2 --stdout --capture-movie | \
ffmpeg -i - -vcodec rawvideo -pix_fmt yuv420p -threads 0 -f v4l2 /dev/video0 &
{% end %}

And now we can use gphoto2's screet to generate udev rules for us:

```bash
#                   magic script                                     script name
/usr/lib/libgphoto2/print-camera-list udev-rules version 0.98 script gphoto2-start-capture | \
sudo tee /etc/udev/rules.d/90-libgphoto2.rules
```

The script will be called when you connect any camera, which is nice IMO.

### Semi-automation

Running the capture all the time consumes CPU, so maybe it's not something you want.
Worry not, we can still make a nice button to start capture in KDE.

Go to `Setting → Removable Storage → Device Actions`.
We're going to add two new actions:

{{ img(src="start_capture.png", comment="Start Capture action") }}

{{ img(src="stop_capture.png", comment="Stop Capture action.
Killing `gphoto2` works just fine :)") }}

After relogin and reconnecting the camera, you will see these two new action in the device menu:

{{ img(src="device_actions.png", comment="The created action in Device Menu") }}

> **NB**: If you choose this approach, remember to undo the ["Full automation"](#full) step by either removing `/etc/udev/rules.d/90-libgphoto2.rules` or commenting out all the lines in `/lib/udev/gphoto2-start-capture`

## Mount

There is not much use for a camera that which lies on your table.
We need a proper mount for it!

After some research, I found [RAM Mounts](https://rammount.com).
They are modular, so you can build anything you need for your desk setup.
They also have two types of labels on each part: either "PATENTED" or "PATENT PENDING".
Probably this is the reason why they are so damn expensive.

Luckily, after some more research, I found [Arkon Mounts](https://www.arkon.com), which don't have these labels and are twice cheaper.

And now I have this amazing attachment to my monitor mount:

{{ img(src="IMG_20230505_072042.jpg, IMG_20230505_072031.jpg", comment="My setup") }}

## Battery

If you're following this guide, pretty soon you'll find out that the camera's battery dies very fast: in about two hours.
Charging it after every video call is pretty cumbersome, and after a couple of months the capacity will be reduced significantly.

The last piece of the puzzle it a dummy battery (bought [here](https://www.ebay.com/itm/185700722680)):

{{ img(src="dummy_battery.jpg" )}}

I _think_ that the intendant purpose is to power you camera from a "Power Bank", but we can just plug it into any USB port.

## Did it worth it?

{{ img(src="compare_day.png", comment="Integrated camera VS DSLR camera during the day.") }}
{{ img(src="compare_night.png", comment="Integrated camera VS DSLR camera during the night with low light.<br/>The difference is mush more drastic.") }}

Overall, I'm happy with the result.
I hate buying new things when I already posses something that has the required capabilities.

I had to buy two new things though: a "dummy battery" and a mount, but both of them can be used for other purposes.
