+++
title = "How to control your PC from a TV remote?"
date = 2021-08-24
updated = 2023-04-22
draft = false
[taxonomies]
tags = ["how-to"]
+++

I have a modern LG TV connected to a PC via HDMI.
I use this setup for coach-based gaming and watching movies.
However, the latter has a major usability issue: you can't just pause playback with a TV remote, you need a mouse or a keyboard nearby.
Because of this I have to use a wireless mouse and keyboard, but there should be a better way of doing this, right?

<!-- more -->

## CEC

And there is! It is called "[Consumer Electronics Control](https://en.wikipedia.org/wiki/Consumer_Electronics_Control)" or CEC for short.
It is a part of the [HDMI specification](<https://en.wikipedia.org/wiki/HDMI#Consumer_Electronics_Control_(CEC)>).
In fact, there is a **mandatory** pin in every HDMI cable just for this feature!

{{ img(src="https://upload.wikimedia.org/wikipedia/commons/4/48/HDMI_Connector_Pinout.svg", comment="Pin 13: CEC") }}

Different companies try to sell CEC under different names: _Anynet+_ (Samsung), _Aquos Link_ (Sharp), _BRAVIA_ (Sony), _EasyLink_ (Philips), _Fun-Link_ (Phillips) and in my case _SimpLink_ (LG).

In TV settings I had to mark my PC as "Set-Top Box", so that TV would imagine that it is supremer and feel the power to send commands to such a peasant device.
But this is only the smaaalest part of the problem.

## Graphics Cards 😓

In order to get it work, both devices should support CEC of course.
I had some doubts about TV or HDMI cable, but as long as the signal is in the PC, everything should be just a matter of drivers and configuration, right? He he he

Even though _there is dedicated physical pin_ for this, almost ALL video cards tend to ignore it.
Mine wasn't any different.

{{ img(src="linus.webp", comment="My feelings at this moment") }}

> If you didn't know, the image above is from Linus Torvald's [talk](https://www.youtube.com/watch?v=MShbP3OpASA) at Aalto University
> "Fuck you, Nvidia!" - he said there

Anyway, we're engineers, we got to solve this!

## Raspberry Pi?

Unlike Nvidia graphics cards, Raspberry Pi that costs ~$30 supports CEC.
The initial idea was to just plug it into USB and use as CEC-USB-adapter.
It would capture the CEC commands from HDMI input, send them via USB while forwarding the rest of video input straight into an output.
That part could even work, however, Raspberry Pi would need two HDMI ports: one for TV input, the other — for PC output.
And it even has two HDMI ports, but... both of them are outputs 😭

Okay, we shouldn't give up... not yet! There is an extension module for Raspberry that acts as a HDMI input: [B101](https://auvidea.eu/b101-hdmi-to-csi-2-bridge-15-pin-fpc/).
Unfortunately, it supports only up to 1080p video, while I have 4K 😭

{{ img(src="https://auvidea.eu/images/auvidea/products/bridge/B101/B101_test_setup_RPi_600x350.jpg", comment="Raspberry Pi with B101 installed on top")}}

This approach will also require _some_ codding, linux configuring and I think there will be some addition latency, which is already on its upper limit (I play Dark Souls 3 right now through 15m cable.
Of course I blame the input lag for my deaths!)

## Pre-made adapter

Luckily, there is a ready-made adapter exactly for this purpose: [Pulse-Eight USB-CEC Adapter](https://www.pulse-eight.com/p/104/usb-hdmi-cec-adapter).
Folks behind it maintain [libcec](https://github.com/Pulse-Eight/libcec), which is an Open Source and available for all platforms.
The total price is around €50 including delivery, which is probably reasonable.

{{ img(src="adapter.jpg", comment="Pulse-Eight USB-CEC Adapter") }}

I got one for myself, and was playing with it for the last few months.
The software part is a bit on a rough side: adapter is not always detected and I had to tweak a lot of settings to make it "just work" in the background without annoying me all the time.

You can bind a few (~6) buttons on your remote to various keyboard buttons, including the "Media" ones.
The most useful functions are `Play/Pause`, `Next/Prev` and Volume.
This allows you to stop thinking how the current video is being played — on PC or on TV itself, and be able stop it the same way in both cases, without reaching for keyboard or mouse.

I'm quite happy with the result.
It's nothing major, but a small convenience thing.
Did it worth its money though? Probably not.
As I said, the experience is a bit rough and while it takes away one annoyance, it introduces another one.
I will continue using the adapter though.
