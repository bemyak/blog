+++
title = "Linux should retire"
date = 2021-03-21
toc = true
draft = true
[taxonomies]
tags = ["linux","containers","thoughts"]
+++

I use Linux from around 2009, and now it is my main operating system, my daily driver and I can't imagine using anything else - it would be a HUGE productivity loss.
However, critical thinking requires me to reevaluate such things constantly, so I started to chew a thought: what is BAD in it? Not obvious things, like poor desktop experience, troublesome graphics or anything that could be fixed with some config tickling.

In addition to that I had to learn some Kubernetes recently and so my dislike of containers in general and Docker in particular became even stronger.
So I'll start with a Docker/Kubernetes rant :)

<!-- more -->

> **Disclaimer** I use term "Linux" to refer to Linux kernel and "GNU\Linux" operating system.
> Please infer the real meaning from the context and don't be peaky.

## Why do we need Docker?

And do we need it at all?

I took these arguments for using docker from [Kubernetes 101](https://www.youtube.com/playlist?list=PL2_OBreMn7FoYmfx27iSwocotjiikS5BD) YouTube series ([timestamp](https://youtu.be/AHDrejEv0SM?t=560)):

* Portability
* Isolated
* Consistency
* Lightweight

Let's dive into each one of them in details.

### Portability

I see this point as a main headliner or all the Docker marketing campaign.
But seriously, what?

To run docker container you need docker installed. If you're not on Linux, you have to use a VM (or WSL), which is _not exactly what Portability means_.

> Java is portable.
> Containers are not.

On Linux thought it helps with:

* Dependencies
* Dynamically linked libraries
* Environment variables

But really, was that really an issue?

Dependencies are handled by package manager for ages.
If you don't want dynamic linking, please compile static binaries.
And for fuck's sake, don't use environment variables, use config files are explicit command line arguments.

And since Macs on ARM is a thing now, I guess nobody should talk about portability seriously, right? It turned out that you need to rebuild all your containers for a new architecture.

### Isolated

This is also a solved problem.
If you don't want to use a VM (that's what Docker is doing on 2/3 of operating systems), there is a shit ton of tools available to you:

* Users/Groups
* Access Control Lists
* SELinux
* Chroot
* Systemd sandboxing

### Consistency

Jeff doesn't talk much about it, but I read it as you can type `docker run` or `docker-compose up` anywhere, and it will just work.
Right?

If you think so, you've never developed compose files that that work both on Mac and Linux.
Docker by itself does work though, but again, we could all just agree to use `make`.

### Lightweight

I'll play my "Docker is a VM on 2/3 of supported OSes" card again here.
Yes, on Linux it is lightweight, but it does bring some costs though.
Take a look at a list in [Isolated](Isolated) section above, that stuff is free!

## Why do people use docker then?

It puts all this benefits together and make them work across multiple distributions and packs into a simple yet powerful format.

* ~~Some~~ most people hate SELinux, it is too complicated
* ~~Some~~ most people don't use sandboxing in Systemd, since it is optional
* Some people use programming languages that have problems with dependency management (e.g. Python)

Docker is a modern way to build "JAR" from anything.
And Kubernetes is good old application server (e.g. Weblogic, Websphere). Nothing new really!

## What does it have to do with Linux?

Yes, this is the point of this post:

> Docker and Kubernetes exist because Linux **has failed**

What?

OK, let's take a step back.
Why does kernel do in the first place?
Among other stuff, the primary goals are:

* resource management: we have only a few CPU cores and hundreds of processes, so we need a scheduler
* process isolation: we don't want one process to read or write into memory of another

Does it ring a bell?




* user separation is not really needed nowadays
* we use it instead of a proper sandboxing
* docker and kubernetes are just consequences of this
  * kubernetes could be replaced with rather stupid manager that can "ssh run command" and reconfigure load balancer
* sandboxing
  * Explicit permissions for each individual binary (manifests?)
* overcomplicated, there are areas that haven't been touched in ages
* automatic clustering?
  * wireguard for connectivity
* portability
