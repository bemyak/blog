+++
title = "Rolling like a Pro"
date = 2020-08-11
draft = false
[taxonomies]
tags = ["rpg", "how-to"]
+++

With the pandemic going on many tabletop role-playing parties migrated online, including ours. I'm a DM, so I struggled a lot with **Roll20**, trying to:

1. Quickly create encounters
1. Have minimal delays during the game
1. Provide good experience for players and me

Here I'm sharing what I've come up with so far.

<!-- more -->

I'm using a free Roll20 account, so some of this is not applicable for paid accounts (probably, don't know for sure), but at least some stuff should work just fine.

## The Problem

As a DM, you control monsters. Lots of them. All with a bit of different abilities, statistics, and role in the game. And you'll need to create them in-game somehow.

Roll20 has two entities that represent a creature: a _token_ and a _character sheet_. They can be connected, so when you change, say, max HP in your character sheet, the token bars will be updated as well. But that is it.

You have two options how to create a creature:

1. Create a character sheet and a token\
   This sucks because creating character sheets is a veeery long process
1. Create only a token\
   This sucks because you lose all the automated rolls and other useful stuff that roll20 gives you

## What does a DM want?

To get the best of the two worlds, of course! Get all the automation while keeping stat block open in a book or somewhere else (website, PDF, etc.)

All the monsters are different, but features that are needed for automation are all the same:

- HP
- Initiative. Usually, it is just a plain bonus.
- Attack rolls. Most monsters have two of them, three at max.

Quite a few, right? It would make sense to put them just in a token, but roll20 can't do that (except HP).
Could we trick it to do that still? **YES!**

## Idea

An empty _edit token_ dialog looks like this:
{{ img(src="empty_edit_token.png", alt="Empty Edit Token") }}

There are not so many fields there (unfortunately).
We're interested mostly in `Bar` fields.

> Luckily, these fields **don't have any validation**, so we can put anything into them

The idea is to put there some text that would be extracted and used by _a macro_.

Accessing those bar fields is pretty easy: e.g. to get the value from _current hp_ bar you can use select a token and then `@{selected|bar1}`.\
To get a value from _maximum hp_ bar use `@{selected|bar1|max}`.

If used in a macro, these placeholders (`@{...}`) will be replaced by the value in a bar and then interpreted just a regular macro text.
So, if we'll put `[[d20+5]]` in a bar field, **the macro will roll that die**!

## Success receipt

First, let's create a set of **macros**:

|Name        |In Bar?|Token Action?|Macros                                                                                   | Comment                                                     |
|------------|-------|-------------|-----------------------------------------------------------------------------------------|-------------------------------------------------------------|
|Attack1     |☐      |🗹           |`/emas @{selected\|token_name} tries to hit @{target\|token_name} @{selected\|bar2}`     | First basic attack                                          |
|Attack2     |☐      |🗹           |`/emas @{selected\|token_name} tries to hit @{target\|token_name} @{selected\|bar2\|max}`| Second basic attack                                         |
|Attack3     |☐      |🗹           |`/emas @{selected\|token_name} tries to hit @{target\|token_name} @{selected\|bar3}`     | Third basic attack if the field contains it                 |
|Special     |☐      |🗹           |`/emas @{selected\|token_name}  @{selected\|bar3}`                                       | Special text if the field contains it                       |
|Initiative  |☐      |🗹           |`/gmroll [[1d20+@{selected\|bar3\|max}&{tracker}]]`                                      | Roll an initiative for a creature and add to combat tracker |
|Roll        |🗹      |☐           |`[[1d20+?{modifier\|0}]]`                                                                | Show a window to put a modifier and rolls d20. Useful when the monster is asked to roll a _Saving throw_ or an _ability check_|

Next, we need to put something in the **token's bar fields**:

- The top bar (green) is used to track the monster's HP.
- The next one (blue) is usually used to track armor or temporary HP, but we can live without that. Instead, we can put some attacks there:
  - `@{selected|bar2}` usually contains something like `with a fist [[d20+11]] (agile, reach 10ft): [[2d6+4]] bludgeoning`
  - `@{selected|bar2|max}` is used for a second attack: `with a jaws [[d20+11]] (reach 5ft): [[1d8+4]] piercing`
- We're running out of fields, so we can use the third bar (red) to store the third attack and initiative
  - `@{selected|bar3}` keeps the third attack (as above) **OR** a special ability text like `Breaths with fire: [[6d6]] fire damage, save DC 25`
  - `@{selected|bar3|max}` stores the initiative bonus, like `5`

The result looks like this:
{{ img(src="token_actions.png", alt="Token Action in action") }}

When the token is selected, this menu is displayed. After clicking on, say, `Attack1` you'll need to select an attack target and then this lovely message is printed for everyone in the chat:
{{ img(src="roll_result.png", alt="Naunet 1 tries to hit Ilona with jaws 24 (chaotic, magical, reach 10 feet): 24 piercing plus 5 chaotic") }}

Perfect!

## Pipeline

After creating the macros, there are not many things left to do to create a new monster:

1. Get somewhere a token
1. Set HP
1. Set Initiative bonus to `Bar3 max`
1. Use `Bar2`, `Bar2 max`, `Bar3` to create an attacks (e.g. `with a fist [[d20+11]] (agile, reach 10ft): [[2d6+4]] bludgeoning`)
1. During the game just select tokens and press buttons! Simple and intuitive

## Problems

Yeah, there are few, but not so critical ones

1. `Bar2` and `Bar3` are now displayed above the creature's head showing some random values. Just ignore them :)
1. You'll need to copy-paste attack values a lot because the bar field is so small
1. You'll need to remember in what field what attack is stored. So just keep them in the same order they are presented in a stats block

## Bonus

The default Roll20 interface is glitchy, messy, and costly.
If you're fine with legally questionable stuff, you'll probably want to take a look at these projects:

1. [VTT Enhancement Suite](https://ssstormy.github.io/roll20-enhancement-suite/) which is a plugin for a browser to fix most of roll20 issues, and add tons of useful tweaks.
1. [Better20](https://get.5e.tools/), which is a script for [Tamper Monkey](https://www.tampermonkey.net/). It requires you to have #1 installed first and add even more cool stuff! Check out the full [feature list](https://wiki.5e.tools/index.php/Feature:_BetteR20). Installation instructions can be found [here](https://wiki.5e.tools/index.php/BetteRoll20_Install).
