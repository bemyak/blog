+++
title = "Looking for a perfect file storage"
slug = "file-storage"
date = 2023-05-18
[taxonomies]
tags = ["how-to"]
+++

I've been searching for an ultimate file hosting for quite some time and finally found it! Let's go through this journey together.

Our goal is to find a file storage solution which is:

- Cheap
- Reliable
- Will be alive in 10 years if left unattended

It would be a dream, right?

<!-- more -->

> This post is (unfortunately) not sponsored

## Own server

Back when I lived in Russia, I managed to make a contract for a public static IP address, which cost me about $2, one-time payment.
It would be a crime **not to** have a personal server set up at home afterwards.

For the next couple of years the server was constantly evolving.
I ended up with a thin HP PC, attached to a {{ hint(text="DAS", hint="Direct-attached storage, basically a disk rack with a USB connector") }} with ~8Tb of disk space.
It run Debian and LOTS of services: LDAP, dovecot and postfix, Nexcloud, Gitlab, FreshRSS, Plex, Samba, Avahi, Cups, hostapd, you name it.

Administrating the server was a lot of fun, and I learned so much in the process! This knowledge allowed me to find my first job relatively easy and it kept boosting me further.

However, I had to move away and left my beloved "Shervik" behind. It kept running for more than a year, until the hard drives have died.
On average, they last for about [four years](https://www.backblaze.com/blog/backblaze-drive-stats-for-q1-2023/), and if they die, they usually do so all at once, so I'm unsure if the data is recoverable.
We'll see when I get there someday.

And this bring us the the main issue with running an own server: **it completely depends on you**.
If you're not there, it's dead.

If you have your own house with a basement, no health issues, the situation in your country is positive, electricity and internet providers are reliable, then sure, go for it.
Unfortunately, I can't tell where I'll be in 5 years from now, so I had to look for something else.

## Dropbox

It's the cheapest of _"public cloud storages"_, offering 2Tb for about €17-20/month.
I used it for two years and migrating from Nextcloud, written by amateurs, to the established industry standard with tons of corporate money poured in it, the best developers improving it full-time, and a decade of continuous evolution was... underwhelming, to say the least.

You can't configure where your photos are uploaded, you can't configure how your "Family Room" is named.
And both of these folders will get their names depending on a system locale, so if your family member has a different locale, you won't be able to dump your photos into the same folder.

The desktop apps on Windows and Linux are terrible.
If you click the tray icon, you are presented with a completely useless view or recently changed files.
To configure what to sync you have to open settings, which welcomes you with an ugly interface that was never revised.

(TBH, the latest Nextcloud version took some inspiration from it, and also introduced this useless deck, which duplicates some functions from the normal "Settings" window).

The web interface is surprisingly slow.
Opening a folder takes unreasonable amount of time, and only suitable for one-time operations, it's not _the way_ of interacting with Dropbox.

Moreover, it randomly decides **not to sync** some folders if they're too big, even if you've explicitly told it to sync everything.
You'll have to find every such folder on your own and enable syncing with RMB menu.
Very convenient.
What's even more bizarre, it that it _allocates space_ for all these files, it's just saving some bandwidth not downloading them :facepalm:

## Hosted Nextcloud

In desperation, I started looking back at Nextcloud, but this time I wanted someone to take care of it for me, after all that's the kind of situation the money were invented for in the fist place.

There are a few Nextcloud _providers_ listed [here](https://github.com/nextcloud/providers#providers).
The prices vary drastically, but overall:

1. they're more expensive than Dropbox
2. who knows if they'll still be around in 10 years.

## Hosting Nextcloud on your own

_"Okay, I can do it myself"_, I thought.
_"We have these clouds nowadays, and I heard S3 is **really** cheap."_

The plan was to run a VM/Container with Nextcloud and point it to some S3-compatible storage.
Amazon is out of the question, since they are assholes, but Oracle offers free VMs.
Unfortunately, both Oracle's and Azure's S3 storage cost like ~€50/month for 2Tb, which is way more expensive than Dropbox.
Nah.

I found [iDrive e2](https://www.idrive.com/object-storage-e2/), which is only $4/Tb/month, but the nearest data center was in Frankfurt, while I have already created my Oracle resources in Zurich.
The resulting bandwidth was unimpressive, and an attempt to recreate my Oracle account in Frankfurt resulted in it being locked in a weird state, so I kept looking for other options.

## Hetzner

I asked a fellow colleague for an advice and he pointed to Hetzner, which is a hoster with servers in Frankfurt and Finland. The similar solution (VM + Storage) costs something like {{ hint(text="10€", hint="~4,70…8,05€ for VM + 3,97€ for Storage") }}!

However, checking their website I stumbled upon a mysterious [Storage Share](https://www.hetzner.com/storage/storage-share) service.
After peeling off the marketing BS, I realized that it's just what I was looking for: a hosted Nextcloud instance, with backups and stuff.
They even let you use a custom domain name and run a set of `occ` commands.

The are two drawbacks though (yeah, nothing is perfect):

1. It is hosted only in Frankfurt, so latency isn't as great as it could be
2. The performance of the underlying VM is pretty lame, so it's not very responsive.

But hey, we can live with it.
€5,32 / month for 1Tb is an amazing price :tada:
