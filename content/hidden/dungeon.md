+++
title = "Dwarf Dungeon"
date = 2021-01-03
toc = true
draft = false
insert_anchor_links = "left"
+++

The idea of a small game for a family.

<!-- more -->

## Features / Principles

1. The game is played by sessions
1. Each session should be 15-20 minutes of time
1. One of the players takes a role of a Dungeon Master
    1. Dungeon master controls various things:
        1. NPCs
        1. ?Traps?
        1. ?Mechanisms?
    1. Players control only one character
        1. It can move around by tapping on a grid
        1. It can interact with objects by tapping on them
        1. It can attack enemies
1. Dungeon master selects a **map**
    1. It consists of hexagons
    2. Map should be created beforehand
    3. Map should have an intro that describes the goals that players should achieve
    4. Map should also have two extra messages: for success and failure
