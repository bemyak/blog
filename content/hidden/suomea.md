+++
title = "Suomea"
date = 2023-05-09
draft = false
+++

## k-p-t vaihtelu

| vahva ↑  | heikko ↓ | esim.          |
| -------- | -------- | -------------- |
| kk       | k        |                |
| pp       | p        |                |
| k        | -        |                |
| p        | v        |                |
| t        | d        |                |
| nk       | ng       |                |
| nt       | nn       |                |
| mp       | mm       |                |
| lt       | ll       |                |
| rt       | rr       |                |
| uku      | uvu      | puku → puvut   |
| yky      | yvy      | kyky → kyvyt   |
| lk(i\|e) | lje      | jälki → jäljet |
| rk(i\|e) | rje      | järki → järjet |

## Verbi

### Verbityppi

| #   | Indikaattori          | Muutos     | Vaihtelu | Infinitivi | Vartalo   |                                  |
| --- | --------------------- | ---------- | -------- | ---------- | --------- | -------------------------------- |
| 1   | `-a/ä`                | loppu pois | ↓        | maksaa     | maksa-    | **hän-persoonassa ei vaihtelu!** |
| 2   | `-da/dä`              | loppu pois |          | juoda      | juo       |                                  |
| 3   | `S` + la/ta/na/ra/sta | `S`e       | ↑        | tulla      | tule-     | juosta → juokse-                 |
| 4   | `V`+ta/tä             | `V`a/ä     | ↑        | haluta     | halua     | maata → makaa                    |
| 5   | `i`+ta/tä             | `i`tse     |          | häiritä    | häiritse- |                                  |
| 6   | `e`+ta/tä             | `e`ne      |          | vanheta    | vanhene-  |                                  |

### 3. infinitiivi

> He vartalo (↑) + maan / massa / masta

### Verbaalisubstantiivi (-minen)

> He-vartalo + minen

| #   | Infinitiivi | Verbaalisubstantiivi |
| --- | ----------- | -------------------- |
| 1   | lukea       | lukeminen            |
| 2   | syöda       | syöminen             |
| 3   | jutella     | jutteleminen         |
| 4   | tavata      | tapaaminen           |
| 5   | häiritä     | häritseminen         |
| 6   | lämmetä     | lämpeneminen         |

### Imperfekti

> Minä vartalo + taika + i

| #   | Muutos | Infinitiivi | 1. pronomini | Imperfekti |
| --- | ------ | ----------- | ------------ | ---------- |
| 1   | +i     | katsoa      | katson       | katsoin    |
| 2   | VV→Vi  | juoda       | juon         | join       |
| 3   | e→i    | tulla       | tulen        | tulin      |
| 4   | -si-   | haluta      | haluan       | halusin    |
| 1→4 | -si-   | tietää      | tiedän       | tiesin     |
| 5   |        | tarvita     | tarvitsen    | tarvitsin  |
| 6   |        | vanheta     | vanhenen     | vanhenin   |

#### Negatiivinen imperfekti

> Infinitiivi vartalo (indicatiivi pois) + nut/nyt/neet

> **Huom!** Assimilatio: pestä → pes- → pessyt; mennä → men → mennyt

| #   | Infinitiivi | Vartalo | Neg. imperfekti  |
| --- | ----------- | ------- | ---------------- |
| 1   | lukea       | luke-   | en lukenut       |
| 2   | syödä       | syö-    | en syönyt        |
| 3   | tulla       | tul-    | en tullut        |
| 4   | tavata      | tava-   | en tava**nn**ut  |
| 5   | tarvita     | tarvi-  | en tarvi**nn**ut |
| 6   | lämmetä     | lämme-  | en lämme**nn**yt |

### Perfekti

> Infinitiivi vartalo (indicatiivi pois) + nut/nyt/neet

Olla + nut/nyt/neet

- olen syönyt
- olet syönyt
- on syönyt
- olemme syöneet
- olette syöneet
- ovat syöneet

#### Negativinen perfekti

ei + ole + nut/nyt

### Pluskvamperfekti

oli + nut/nyt/neet

#### Negativinen pluskvamperfekti

ei ollut/olleet + nut/nyt/neet

### Konditionaali
| | | | |
| - | - | - | - |
| presens  | he-vartalo (e,i pois) (V¹V²→V²) + isi | nukkuisin       | en nukkuisi       |
| perfekti | olisi + nut/nyt/neet        | olisin nukkunut | ei olisi nukkunut |
| passiivin presens | (1) _minä_-vartalo + ttaisiin/ttäisiin <br/> (2,3) infinitivivartalo + taisiin/täisiin <br/> (4-6) infinitivivartalo + ttaisiin/ttäisiin |  nukuttaisiin | ei nukutaisi |
| passiivin perfekti | (ei) olisi + tu/ty/ttu/tty | olisi nukuttu | ei olisi nukuttu |

### Passiivi

| | | | |
|-|-|-|-|
| presens    | (1) _minä_-vartalo + taan/tään <br/> infinitiivi + an/än | nukutaan | ei nukuta |
| imperfekti | (1) _minä_-vartalo + ttiin/ttu/tty <br/> (2,3) infinitiivi vartalo + tiin/tu/ty <br/> (4,5,6) infinitivi vartalo + ttiin/ttu/tty | nukutiin | en nukuttu |
| perfekti   | on / ei ole + tu/ty/ttu/tty | on nukuttu | ei ole nukuttu |
| pluskvamperfekti | oli / ei ollut + tu/ty/ttu/tty | oli nukuttu | ei ollut nukuttu |


### Imperatiivi

| | | |
|-|-|-|
| sinä | (1) _minä_-vartalo | nuku | älä nuku! |
| te   | (1-3) vartalo + kaa/kää <br/> (4-6) + tkaa/tkää | nukkukaa! | älkää nukkuko! | 

### Verbin ohjaus

| verbi      | question      |
| ---------- | ------------- |
| rakasta    | mitä          |
| tutustua   | mihin         |
| viihtyä    | missä         |
| ihastua    | mihin         |
| vilmistua  | mistä / miksi |
| osallistua | mihin         |
| kiinnostn  | mistä         |
| pitää      | mistä         |

## Sanatyypit

### Partitiivi

`V` + a/ä
`VV/K` + ta/tä
`e` + tta/ttä

### Illatiivi (Mihin?)

1. `V` → `VV`n koulu → kouluun
2. `VV` → `VV`h`V`n maa → maahan
3. pitkä sana `VV` → `VV`seen vapaa → vapaaseen

### Esim.

> Vahva vartalo on: nominatiivissa, partitiivissa, illatiivissa

| Sanatyyppi           | vaihtelu | Nominatiivi | Partitiivi  | Vartalo      |
| -------------------- | -------- | ----------- | ----------- | ------------ |
| -`V`                 | ↓        | matka       | matkaa      | matka        |
| -e                   | ↑        | vene        | venettä     | venee-       |
| -nen                 |          | valkoinen   | vilkoista   | valkoise-    |
| uudet i-sanat        | ↓        | hotelli     | hotellia    | hotelli-     |
| vanhat i-sanat 1 i→e |          | suuri       | suurta      | suure-       |
| vanhat i-sanat 2 i→e |          | jarvi       | jarveä      | jarve-       |
| vanhat i-sanat 3 -si |          | uusi        | uutta       | uuda-        |
| -in                  | ↑        | puhelin     | puhelinta   | puhelime-    |
| -as/äs               | ↑        | lounas      | lounasta    | lounaa-      |
| -is                  |          | kaunis      | kaunista    | kaunii-      |
| -us/ys/os/ös         |          | rakennus    | rakennusta  | rakennukse-  |
| -ut/ut               |          | lyhyt       | lyhyttä     | lyhye-       |
| -nut/nyt             |          | väsynyt     | väsynyttä   | väsynee-     |
| -ton/tön             | ↑        | rasvaton    | rastattonta | rastvattoma- |

| Nominatiivi | Partitivi | Vartalo  | Genetiivi        | Akkusatiivi [^1] | Illatiivi       | Inessiivi       | Elatiivi        | Allatiivi | Adessiivi | Ablatiivi | Monikko |
| ----------- | --------- | -------- | ---------------- | ---------------- | --------------- | --------------- | --------------- | --------- | --------- | --------- | ------- |
| mikä        | mitä      | mi-      | minkä            | mikä / minkä     | mihin           | missä           | mistä           | mille     | millä     | miltä     | mitkä   |
| kuka        | ketä      | kene-    | kenen            | kenet            | keneen          | kenessä         | kenestä         | kenelle   | kenellä   | keneltä   | ketkä   |
| tämä        | tätä      | tä-      | tämän            | tämä / tämän     | tähän / tänne   | tässä / täällä  | tästä / täältä  | tälle     | tällä     | tältä     | nämä    |
| tuo         | tuota     | tuo-     | tuon             | tuo / tuon       | tuohon / tuonne | tuosse / tuolla | tuosta / tuolta | tuolle    | tuolla    | tuolta    | nuo     |
| se          | sitä      | si-/sii- | sen / siis       | se / sen         | siihen / sinne  | siinä / siellä  | siitä sieltä    | sille     | sillä     | siltä     | ne      |
| ne          | niitä     | nii-     | niiden / niitten | ne               | niihin          | niissä          | niistä          | niille    | niillä    | niiltä    |         |
| minä        | minua     | minu-    | minun            | minut            | minuun          | minussa         | minusta         | minulle   | minulla   | minulta   | me      |
| sinä        | sinua     | sinu-    | sinun            | situt            | sinuun          | sinussa         | sinusta         | sinulle   | sinulla   | sinulta   | te      |
| hän         | häntä     | häne-    | hänen            | hänet            | häneen          | hänessä         | hänesta         | hänelle   | hänellä   | häneltä   | he      |
| me          | meitä     | mei-     | meidän           | meidät           | meihin          | meissä          | meistä          | meille    | meillä    | meiltä    |         |
| te          | teitä     | tei-     | teidän           | teidät           | teihin          | teissä          | teistä          | teille    | teillä    | teiltä    |         |
| he          | heitä     | hei-     | heidän           | heidät           | heihin          | heissä          | heistä          | heille    | heillä    | heiltä    |         |
| joka        | jota      | jo-      | jonka            | joka/jonka       | johon           | jossa           | josta           | jolle     | jolla     | jolta     | jotka   |
| jotka       | joita     | joi-     | joiden           | jotka            | joihin          | joissa          | joista          | joille    | joilla    | joilta    |         |

[^1]: Objecti. Kun objecti on persoonapronomini, e.g., Ajan sinut autolla lentoasemalle

### Moniko

| #   | sääntö                                       | vartalo   | monikkovartalo |
| --- | -------------------------------------------- | --------- | -------------- |
| 1   | o,ö,u,y<br/>pysyvät                          | talo-     | taloi-         |
| 2   | a,ä,e<br/>menevät pois                       | juna-     | juni-          |
| 3   | a+i → oi<br/>kun 2 tavua ja 1. tavussa i,e,a | kiva-     | kivoi-         |
| 4   | i+i → ei                                     | bussi-    | bussei-        |
| 5   | VV → Vi<br/>sama V tai uo,yö,ie              | maa-      | mai-           |
| 6   | sanatyyppi `si`<br/>ei vaihtelua             | uusi-     | uusi-          |
| 7   | sanatyyppi Vus/Vys<br/>us/ys → ksi           | rakkaude- | rakkauksi-     |

## Täytyy-lause

> Genetiivi + täytyy / ei tarvitse + ininitiivi / partitiivi

- Minun täytyy opistella
- Pedron ei tarvitse mannä lääkärille
- Meidän täytyy ostaa auto
- Minun ei tarvitse siivota keittiötä
- Sinun täytyy muistaa hänet

## Objekti

Partitiivi kun:

- Lause on negatiivinen _En osta uutta sanakirjaa._
- Objekti tarkoittaa ainetta (ainesanat, abstraktisanat) _Anna vettä!_
- Verbi ilmaisee toimintaa, joka on vielä kesken. Ei ole loppu. _Pirjo luki kirjaa_ vs _Pirjo luki kirjan._
- Lauseessa on ns. partitiiviverbi _Odota minua!_

## Komparatiivi

|  | | N | P | V | ILL |
|--|-|---|---|---|-----|
| Adj Komparatiivi Yks | GenV+`mpi`   | nopeampi  | nopeampaa  | nopeamma-  | nopeampaan  |
| Adj Komparatiivi Mon | GenV+`mmat`  | nopeammat | nopeampia  | nopeammi-  | nopeampiin  |
| Adj Superlatiivi Yks | GenV+`in`    | nopeinin  | nopeinta   | nopeimma-  | nopeimpaan  |
| Adj Superlatiivi Mon | GenV+`immat` | nopeimmat | nopeimpia  | nopeimmi-  | nopeimpiin  |
| Adv Komparatiivi Yks | GenV+`mmin`  | nopeammin |
| Adv Komparatiivi Mon |
| Adv Superlatiivi Yks | GenV+`immin` | nopeimmin |
| Adv Superlatiivi Mon |
