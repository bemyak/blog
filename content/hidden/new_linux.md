+++
title = "Linux TODO list"
date = 2022-04-06
draft = false
+++

- YADM
- ddci
- `sudo localectl set-keymap --no-convert carpalx`

-

{% code (file="/etc/pacman.conf") %}
…

# Misc options

# UseSyslog
Color
# TotalDownload
CheckSpace
VerbosePkgLists
ILoveCandy
…
{% end %}

{% code(file="/etc/modprobe.d/i915.conf") %}
options i915 enable_guc=2 enable_fbc=1 fastboot=1 enable_psr=0
{% end %}

{% code(file="/etc/mkinitcpio.conf") %}
MODULES=(dm_mod dm_crypt ext4 sha256 sha512 intel_agp i915)
BINARIES=(cryptsetup)
FILES=()
HOOKS=(base systemd autodetect modconf block keyboard sd-vconsole sd-encrypt fsck filesystems)
{% end %}

{% code(file="/etc/paru.conf") %}
[options]
…
NewsOnUpgrade
BatchInstall
{% end %}

{% code(file="/etc/environment") %}
LIBVA_DRIVER_NAME=iHD
VDPAU_DRIVER=va_gl
GST_VAAPI_ALL_DRIVERS=1

ELECTRON_TRASH=kioclient5
IDEA_JDK=/usr/lib/jvm/jdk-jetbrains
GTK_USE_PORTAL=1
LESS=-R
EDITOR=vim

MOZ_USE_XINPUT2=1
MOZ_WEBRENDER=1
TDESKTOP_DISABLE_GTK_INTEGRATION=1

GOPRIVATE=gitlab.com
JAVA_HOME=/usr/lib/jvm/default-runtime
ANDROID_SDK_ROOT=/opt/android-sdk
DEBUGINFOD_URLS=<https://debuginfod.archlinux.org/>

RUSTC_WRAPPER=/usr/bin/sccache
{% end %}
