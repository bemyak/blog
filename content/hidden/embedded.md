+++
title = "Embedded Cheat Sheet"
date = 2021-09-30
draft = false
+++

<!-- more -->

## Shell Commands

* `openocd -f interface/stlink.cfg -f target/stm32f3x.cfg` — start a debug proxy
* `itmdump -F -f itm.txt` — read data from itm dump
* `cargo objdump --release --bin registers -- -d --no-show-raw-insn --print-imm-hex --source > release.txt` - generate a file with assembly representation

## GDB Commands

* `load` - upload firmware
* `disassemble /m` - show code with inlined assembly
* `print <something>` - print value of a variable
* `info locals` - show local variables
* `info args` - show current function arguments
* `monitor reset halt` - reboot the controller
* `layout split` / `tui disable` - turn code preview on / off
* `set <var> = <val>` - set value of a particular variable
* `examine <addr>` or `x <addr>` - print value in memory address
* `list` - list current function
* `print/x *ef` - print registers of a snapshot of the program state right before the exception occurred

## VSCode config

{% code(lng="json", file=".vscode/launch.json") %}
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Debug (OpenOCD)",
            "type": "cortex-debug",
            "request": "launch",
            "servertype": "openocd",
            "cwd": "${workspaceRoot}",
            "executable": "${workspaceRoot}/.vscode/program",
            "preLaunchTask": "linkBinary",
            "device": "STM32F303VCT6",
            "configFiles": [
                "interface/stlink.cfg",
                "target/stm32f3x.cfg"
            ],
            "svdFile": "${workspaceRoot}/STM32F303.svd",
            "runToMain": true,
            "swoConfig": {
                "enabled": true,
                "cpuFrequency": 8000000,
                "swoFrequency": 2000000,
                "source": "probe",
                "decoders": [
                    {
                        "type": "console",
                        "label": "ITM",
                        "port": 0
                    }
                ]
            },
            "preLaunchCommands": [
                "set mem inaccessible-by-default off",
                "set verbose on"
            ],
            "postLaunchCommands": [
                "monitor reset run",
                "monitor sleep 200",
                "monitor psoc6 reset_halt sysresetreq"
            ],
            "postRestartCommands": [
                "monitor psoc6 reset_halt sysresetreq",
                "monitor gdb_sync",
                "stepi"
            ],
        }
    ]
}
{% end %}

{% code(lng="json", file=".vscode/tasks.json") %}
{
    "version": "2.0.0",
    "tasks": [
        {
            "label": "build",
            "type": "cargo",
            "command": "build",
            "args": [
                "--target",
                "thumbv7em-none-eabihf",
            ],
            "problemMatcher": [
                "$rustc"
            ],
            "group": {
                "kind": "build",
                "isDefault": true
            },
            "options": {
                "cwd": "${fileDirname}",
            },
        },
        {
            "label": "linkBinary",
            "type": "shell",
            "command": "ln -f -s (cargo build -q --target thumbv7em-none-eabihf --message-format=json | jq -r 'select(.executable) | .executable') ${workspaceRoot}/.vscode/program",
            "options": {
                "cwd": "${fileDirname}",
            },
            "dependsOn":[
                "build",
            ]
        }
    ]
}
{% end %}

## Links

* [Rust Embedded Discovery Book](https://docs.rust-embedded.org/discovery/)
* [How to use GDB](https://docs.rust-embedded.org/discovery/appendix/2-how-to-use-gdb/index.html)
* [Debugging Rust ARM CortexM Programs with Visual Studio Code](https://dev.to/rubberduck/debugging-rust-arm-cortexm-programs-with-visual-studio-code-336h)
* [Debugging Rust Cortex-M with VS Code: Take 2](https://dev.to/rubberduck/debugging-rust-cortex-m-with-vs-code-take-2-248j)
