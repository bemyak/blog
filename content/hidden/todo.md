+++
title = "TODO"
date = 1970-01-01
toc = true
draft = false
insert_anchor_links = "left"
+++

## Things to fix / Implement

1. Latests posts? (<https://zola.discourse.group/t/proposal-question-transparent-modes/699/2>)

## Posts to write

1. ? D&D5e vs PF2e
1. Grafana: How to use Prometheus variables? `($we_instance)`
1. translate-shell
1. VPN in Oracle Cloud
1. Roll Bot
1. How browsers failed the web (HTML vs rendering)
1. Linux should retire
1. Why Rust?
1. DevOps as I see it

## Games

1. Doom, Doom Eternal
1. Dark Souls?
1. Vaporum
1. Haven
1. Prey
1. Elden Ring

## Topics for Telegram Blog

1. Nuuksio
1. 2 years in Finland
    1. language
    1. job
1. Miksi Suomi?
