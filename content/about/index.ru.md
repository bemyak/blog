+++
title = "Hello World"
date = 1991-01-10
draft = false
+++

Привет и спасибо, что зашли!

Чутка обо мне и этом блоге

<!-- more -->

## Что тут есть?

Русскоязычная секция целиком посвящена нашим с Мариной поездкам и путешествиям.

Всё начиналось как [телеграм-блог](https://t.me/me_andshe), но потом посты там стали чем-то большим, нежели потоком фоток. В итоге, я начал готовить их в markdown'е, а там уже не сложно было выложить сюда. Старые посты я довольно тупо скопипастил из телеграмма, в основном для истории и бэкапа.

Если вам больше интересно про IT, игры и прочее, то загляните в [англоязычную версию сайта](@/_index.md)

Что тут можно найти:

<div class="is-flex is-flex-wrap-wrap-reverse is-justify-content-space-between ul-empty">

- [x] Жалобы на жизнь
- [x] Фотки
- [x] Немного карт
- [x] Чуть-чуть видео
- [x] Ужасное мыслеизложение
- [x] Грамматические ошибки
- [x] Кривая вёрстка
- [ ] Что-то полезное

{{ img(src="ava.png" alt="My photo" height=250, click=false) }}

</div>

## Как работает этот блог?

Про это есть [отдельный пост](@/dev/blog/index.md) (на английском)

## Как со мной связаться?

|                                                         |          |                                           |
| ------------------------------------------------------- | -------- | ----------------------------------------- |
| ![Telegram](https://telegram.org/img/favicon-32x32.png) | Telegram | [bemyak](https://t.me/bemyak)             |
| ![Email](https://proton.me/favicons/favicon-32x32.png)  | Email    | [blog@bemyak.net](mailto:blog@bemyak.net) |
| ![GitLab](https://gitlab.com/favicon.ico)               | GitLab   | <https://gitlab.com/bemyak>               |
| ![GitHub](https://github.com/favicon.ico)               | GitHub   | <https://github.com/bemyak>               |
