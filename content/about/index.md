+++
title = "Hello World"
date = 1991-01-10
draft = false
+++

Hello and welcome to my blog!
Here you'll find some blog description and contact details

<!-- more -->

## What can you expect to find here?

<div class="is-flex is-flex-wrap-wrap-reverse is-justify-content-space-between ul-empty">

- [x] Life complains
- [x] Technology depression
- [x] Rust love
- [x] Unfinished projects
- [x] Bad ideas that should never be implemented
- [x] Logs of my failures
- [ ] Any useful stuff

{{ img(src="ava.png" alt="My photo" height=250, click=false) }}

</div>

## How to contact me?

|                                                             |                   |                                                                      |
| ----------------------------------------------------------- | ----------------- | -------------------------------------------------------------------- |
| ![Protonmail](https://proton.me/favicons/favicon-32x32.png) | Email (preferred) | [blog@bemyak.net](mailto:blog@bemyak.net)                            |
| ![GitLab](https://gitlab.com/favicon.ico)                   | GitLab            | <https://gitlab.com/bemyak>                                          |
| ![GitHub](https://github.com/favicon.ico)                   | GitHub            | <https://github.com/bemyak>                                          |
| ![Mastodon](https://joinmastodon.org/favicon-32x32.png)     | Mastodon          | <a rel="me" href="https://lor.sh/@bemyak">https://lor.sh/@bemyak</a> |
| ![LinkedIn](LI-Logo.png)                                    | LinkedIn          | <https://www.linkedin.com/in/bemyak/>                                |

## How does this blog work? It's awesome

There is a [post](@/dev/blog/index.md) about it!

## "How can I support you?"

Thanks =)\
Please, consider [buying me a coffee](https://ko-fi.com/bemyak)
