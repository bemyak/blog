+++
title = "Outer Wilds"
date = 2024-03-18
[taxonomies]
tags = ["good-game", "review"]
+++

{{ img(src="https://steamcdn-a.akamaihd.net/steam/apps/753640/library_hero.jpg", alt="outer wilds cover") }}

An amazing small game about space and exploration!

<!-- more -->

You might have noticed that most of my reviews here are mostly negative. I think it's because when something is wrong with a game, it's pretty easy to tell what annoyed you. With good games it's much harder to say anything except "THIS WAS GREAT". But I'll still try to go through my favorites and we'll find out what makes them click :)

Outer Wilds is a weird mix of Kerbal Space Program and The Mist. I don't want to spoil it at all, because it's a _"knowledge-based"_ game: once you know what to do, you'll beat it in about 15 minutes. Literally.

The bare minimum I can tell you so that you make any sense out of this post: the game is about a toy solar system, which you can explore on you space ship.

## Bounded scope

The game's world game is tiny. It has nothing auxiliary: every smallest piece of rock is there for a reason. The content is amazingly dense, even when you thought you're done with some area, you will shortly discover more stuff right under (or sometimes above) your nose.

For sure the developers could have added AI generated planets like in Starfield, but what's the point? Exploring them would be as boring and pointless as creating them.

After completing Outer Wilds you know (and love (mostly)) every single piece of it. It starts feeling like home, and frying marshmallows in a bonfire adds a great deal to it.

## The Player's role

The game events just unfold regardless of the player. You can choose to engage with them or not, but they will happen. Don't worry, there's absolutely no rush. The point here is that it makes the game feel _fair_.

In other games villains wait for the Hero to start their quest. Dragons wait to be slaughtered. Princesses wait to be saved. This games just goes on. It's a story, where you need to find the right place for you, and that takes time and practice.

There's no right way to explorer the world. You just see something and go there.

## Physics-Inspired

Our real world is amazing. Black holes, dark matter, quantum physics, relativity theory are mind blowing. Outer Wilds takes a lot of inspiration from the most peculiar subjects on the science frontier and puts them into the dense package of amazingness.

I was often wondering "hmm, is this possible in the real world?", and quite often the answer was really long and it boiled down to "sort of". There are many loooong blog posts and videos online diving deep into the scientific basis of the game.

The basis is there, but authors were not bounded by it. They've added a fair bit of fantastic elements, to create something unique to tickle your mind.

By the way, the physics simulation is done exceptionally well and plays a vital part in the game.

## Cat Killer

Curiosity killed a cat, but at the same time it's one of the greatest human traits. Venturing into the unknown, exploring alien planets, dig out ancient secrets. This game puts many hats onto your head: astronaut, archeologist, historian, digger, diver, detective. All these roles have one in common: they require innate curiosity, and if you have it, the game gets you hooked in no time.

## Outro

For me, Outer Wilds was one of the (if not _the_) best game experiences ever. Made with lots of love from the developers, it's more of an art than a game.

I'm slightly afraid that I might rouse the expectations a bit too high. I saw some people commenting that they don't get all the hype around it.
Anyway, there is only only way to find out if that's your cup of tea or not: forget everything you've read and go play it :)
