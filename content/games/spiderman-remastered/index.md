+++
title = "Spider-Man: Remastered"
date = 2024-03-05
[taxonomies]
tags = ["bad-game", "review"]
+++

{{ img(src="https://steamcdn-a.akamaihd.net/steam/apps/1817070/library_hero.jpg", alt="spider-man: remastered cover") }}

Why I hate AAA games, but still play them.

<!-- more -->

Let's break it down, what we have here:
* Cinematic main quest
* Meditative swinging through the city
* Tons upon tons of filler content with just enough interest sparks to keep you doing it
* Overly-casual battle mechanic

## Main quest

It's like watching a typical Spider-Man movie or a cartoon. Familiar heroes, familiar villains, a familiar city. The acting and cinematic are great, the character development is done by-the-book perfectly.

The only issue for me is that it would rather be a movie, not a game. The player doesn't take any part in the story, it unfolds by itself. It doesn't matter whether all crimes are stopped or the city lies in ruins. If all enemy bases are destroyed. If all clues are investigated. All events, dialogs, endings will be the same, so after beating the game I felt it was pointless to do anything except the main quest.

## Side quests

They are good. They caught my interest, but it's a pity there are so few of them (and they don't affect anything as I've mentioned).


## Traversal

Swinging through the city feels amazing. It puts you kinda in a zen mood, when you are just enjoying the beauty of movement. Reminds me of Alto's Odyssey and similar mobile games.

New moves become available periodically, which changes the way you move. When it gets boring, fast travel is unlocked, but I was still doing lots of web traversal just for fun.

##  Points of interest

There are different kinds of those. Some feel like mini-quests (research  stations), but some are just annoying. Especially the "city crimes". There are **165** in total, which you have to complete to get an achievement. It's bottom line boring.  Plus 55 backpacks, 47 landmarks, 50 secret photo locations (which are not visible on the map)... That's **a lot**!

To not overwhelm the player from the start, the game initially hides all of this. When you complete a main mission, a portion of these points is unlocked around the city. So the game loop boils down to:
1. Complete a story missions
2. Go through all the districts to clean up the points
3. Go to 1

I feel like it was the way to compensate for the relatively short story line. So after 10 minutes of awesome cinematic you get 1.5 hours of grinding.

## Miles Morales

It's the second game, but probably it should be a DLC, so putting it here.

Miles Morales builds on the foundation of the previous game, amplifying both good and bad aspects of it.

The graphics, the city, the visuals, the animations are superior.

The story is again a classical Spiderman story.

What bothered me a little is that the main character is stereotypical "afro-american-puerto-ricanian" boy. Obviously, I don't belong to the same group, but whenever I see "stereotypical Russians" I feel cringe. I guess people whom Miles Morales is supposed to represent feel the same when he listens to stereotypical music,  draws stereotypical graffiti, and has stereotypical problems. It's good if I'm mistaken here and folks are actually OK with this, but anyway stereotypes are bad, don't do them.

City Crimes are now reoccurring events. So after completing them, they will still pop up to bother you even more.

Some of the abilities are now enchanted with electricity. In practice it means that you have to press one additional button to activate them. E.g. `X` + `A` no longer works, only `LB` + `X` + `A`. I managed to beat Armored Core IV where you have to press all the buttons sometimes, but even there it wasn't such a mess.

## Conclusion

Take a very poor game. Add nice graphics, zen traversal, spackle with familiar heroes abs stories, put a few cinematics on top. It's done, the fan base is yours, me including :)
