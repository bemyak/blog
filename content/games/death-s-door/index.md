+++
title = "Death's Door"
date = 2022-09-16
[taxonomies]
tags = ["review", "good game"]
+++

{{ img(src="https://steamcdn-a.akamaihd.net/steam/apps/894020/library_hero.jpg", alt="death's door cover") }}

An excellent small relaxing game that I've enjoyed. It took me 24 hours to get all the achievements I wanted.

<!-- more -->

> Very minor spoilers ahead.

## ➕ Game length

The game feels neither too short nor unnecessarily stretched out.
It takes you a while to learn mechanics and enjoy them while not getting bored.

There is some running around if you want to get the True Ending, but it's not too annoying.

## ➕ Puzzles

There are no puzzles like in [the Witness](@/games/witness/index.md), just some doors to open and traps to pass.
However, these don't feel too stupid.
The game challenges you by setting pretty tight timings or throwing a bunch of tough enemies at you.

It tries to trick your thinking a bit.
I remember a room with a locked door and a key that I couldn't reach since I lacked some ability.
The key to that door turned out to be 5 rooms away, and I wasn't even sure I was supposed to use it there since, in most games, this indicates that you're doing something wrong.
Not in the Death's Door, though 👍

## ➕ Character design

NPCs, enemies and bosses are adorable and unique.
Jefferson is an especially great guy!
Foes are easy to distinguish, and their look gives you a good idea of their move set.

## 〰️ Story

The game setting is beautiful, and the idea of _Office Death Crows_ is fresh and stylish.
The plot makes sense: you have a few narrative "guns" hanging on the wall that fire when the time is right.

My main complaint it the true ending, which came out of nowhere, without an aforementioned "gun" being hung.
It didn't reveal any truth about the world or give new context to events.
It just added a couple of brushstrokes to the begging, instantly resulting in an event at the end.

## ➖ Spells

In particular, spell controls.
The game relies on them A LOT, and you can't just ignore them.

> There are 4 spells, and you can switch the active one with `DPad`.
> To cast a spell, you must press `LT` to start _aiming_ and hold `B` to charge it.
> Finally, release `B` to fire.
>
> A lot of buttons to press, rights?

My issue with the controls was that instead of `B`, I kept pressing `A`, which typically resulted in the crow rolling right off a cliff.

This could've been solved pretty easily if you wouldn't have to switch spells at all!
Each spell could be bound to a dedicated button:

* Arrow to `Y`
* Fireball to `X`
* Bomb to `B`
* Grappling hook to `A`

And simply pressing `LT` + `X` would cast a fireball instantly!

Another issue is the need for "charging" spells.
It results in an inability to use them during combat: someone will put an arrow in you if you stand still for a couple of seconds.

## Conclusion

The game is fantastic and satisfying.
No typical annoyances or common pitfalls that other games have.
Very polished experience.

My take away from here is:

> You don't need intricate gameplay or ingenious puzzles all over the place to make a game enjoyable.
> Sometimes, it is acceptable to leave "boring" sections that don't have any challenge at all.
