+++
title = "Disco Elysium"
date = 2020-08-28
draft = false
[taxonomies]
tags = ["review","good game"]
+++

{{ img(src="https://steamcdn-a.akamaihd.net/steam/apps/632470/library_hero.jpg", alt="disco elysium cover") }}

One of the best game of the 2020 year and a fresh breeze in RPG genre, took me 60 hours to beat. Let's dig into some details...

<!-- more -->

> **MINOR SPOILERS AHEAD**. I warned you.

## Basics

Let's go through the game mechanics and take a closer look!
I'll leave my own opinions in a "quote" if they are just one-liners, or get back to these mechanics later if they deserve a broader discussion.

### Time

The game has a clock that ticks during dialogs or while thinking (i.e. talking to yourself).
It is quite an important resource because some tasks are time-limited, or some events are happening only at a specific time of the day.

Time does not advance when you're just walking around, ~~collecting bottles,~~ exploring the world, but it goes pretty fast when you read.

> I usually highly dislike timers, but this implementation is **fantastic**.
> You do feel a bit of pressure, but you don't need to panic and can just plan your day

### Voices in your head, a.k.a. Skills

{{ img(src="skills.jpg", alt="skills") }}
Your character has a lot of skills, and you can invest a point in them.
You start by setting the basic level of your _skill groups_ (rows on the screenshot above).
You can't level up a _skill group_ later.

The basic level also defines the maximum value of skill, so if you're terrible at something, you can't get any better in it just by leveling up few times.

With each level, you can invest one point in a specific skill. Quiet like in other RPG games, right?

> No. But we'll get back to this later

Each skill is a voice in your head. There are a few more there that you can't directly improve: _Ancient Reptilian Brain_, _Limbic System_, etc.

### Skill checks

{{ img(src="dialog.jpg", alt="dialog") }}
Skill checks that come into play all the time without your concern are called **passive checks**.
I think you can retry them, by performing the same set of actions again (if it is possible), but I'm not sure.

There are also two types of **active** checks:

1. White checks that you will be able to retry in case of failure
1. Red checks for which you have only one attempt

A skill check is performed by rolling `2d6 + skill_modifier`. Snake eyes are always failing and sixes are always winning. Simple and elegant!

### Thought cabinet

{{ img(src="cabinet.jpg", alt="cabinet") }}
You'll get **thoughts** during your walkthrough, and you can _equip_ them in your cabinet.
While _thinking_, you'll get a temporary bonus to your skills (usually _negative_) and a permanent on completing the thought.
You don't need to do anything for your thought to progress, it just takes some in-game _time_.

This is another place where you can spend level-up points:

1. To unlock new thought slots
1. To forget thoughts

### Clothes

> This one is brilliant!

The idea is that you **are smart if you _feel_ smart**. To _feel_ smart you need to dress like a nerd!

So, yeah, your clothes buff some skills and debuffs others. Again, simple and elegant, just as I like :)

### Setting

In order not to spoil too much: the setting is exactly what you would expect by looking at the cover and reading the game's title.
A bit dark retro in a parallel reality.
You're an alcoholic detective, and you've got a case!

> Go! Dance! Disco! Hardcore!

The world is an allusion to the real one with maxed-out ideals and hyperbolic traits. If you like post-USSR themes, you should go for it! If not, well, just bear with it.

Aaaand... That's it! Now you know everything!

### Story

{{ img(src="boat.jpg", alt="boat") }}
I won't say much here, but it is excellent! Unexpected and mystical events so excited me!

> Unfortunately, there are LOTS of politics.
> Every drunk will be trying to discuss ADVANCED CAPITAL THEORY or something like this.\
> This is funny sometimes, and sometimes you can avoid such topics, but starting from some point the main story begins to spin around communism and stuff

## Okay, what's wrong?

The game won [tons of awards](https://en.wikipedia.org/wiki/Disco_Elysium#Awards), and I'd say they are well deserved!
But of course, nothing is perfect and that is why I'm writing this post :)

### RPG is pointless

> What?

Yeah, what a hot take, right? :) Let me explain!

Why do we have skills? To have skill checks!\
Why would we need checks? To give a player _a chance to fail_!\
But why is that required? To make things interesting! Right?

The idea is that **failing** something forces a player to either try later or come up with a different approach.
Unfortunately, Disco Elysium utilizes only the first option, it says to a player: _Just try later. When you'll get a new level or something._

And this is so disappointing! Almost newer there is another way to do something except succeeding is a specific check.

> Failed to **persuade a door to open with a Suggestion**? Bad luck! No, of course, you can't use your GIANT CAN OPENER!

For me it often led to save-load abuse, because I was interested in exploring the world, talking to people, completing quests.
But the game just didn't let me do it! WHY?!

There is a simple test to detect such pitfalls: Imagine that your player is a cheater and started a new game with all stats maxed out!\
Will the game be _more_ or _less_ enjoyable for them?

It is something like [Elex](@/games/elex/index.md), the answer is NO. It would be super boring not to have any challenge at all, and just killing everyone with one hit.

In Disco Elysium? Weeell... You will just get so much more lore and content out of the game! I can't see any reason why you would want to take it from a player.

### Thought cabinet is pointless

The problem is that you don't know beforehand what buff or debuff a complete thought will give to you. And if that was a debuff... You need to spend a skill point to forget it!

The number of slots is pretty small, so to internalize a new thought you need to unlock a new slot first.
So, every new thought costs one skill point: either you put it into a new slot, or forget an old one.

And for me, this is quite expensive! The game gives you a choice: put a point into some skill, or get something random.
Luckily, we have ~~google~~ [DuckDuckGo](https://duckduckgo.com) to check how the thought will affect your character and if it is worth it.

And this is quite disappointing, when I first saw that interface, I was excited to see something like Mind Palaces of Sherlock Holmes, but this turned out to be just another stupid mechanic.
Alas!

### Technical implementation

Yep, still, bugs, no controller support, corrupted save files, one crash occurred for me, flickering shadows, and your companion likes to stand INSIDE YOU.
Thank you, Unity!

## Conclusion

Already? Yes! As I said, the game is good and solid!

Takeaways:

1. Please, verify that your game needs RPG elements. Remove them when they are limiting your players instead of giving them role-playing means!
1. There is no №2, I told you that it is an excellent game, it is just not for everyone :)

Good day, and thanks for reading!
