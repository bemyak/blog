+++
sort_by = "update_date"
title = "Games"
insert_anchor_links = "left"
+++

Here I publish reviews on games I played. Some of these reviews may contain `minor` or `major` spoilers, which is explicitly stated when relevant.

The primary purpose of this is to:

1. Capture my impressions of a game.\
   Memory fades quickly, and it is nice to have a place like this where I can recall my feelings about a game.
2. Analyze and highlight things that I especially liked or disliked.\
   Everybody wants to be a game designer, me included!\
   Having a list of good and bad mechanics, designs, and approaches may be helpful to me.

All of this is 100% subjective, of course. If you don't agree with something — it is fine! You can [reach me out](@/about/index.md#how-to-contact-me) and we can discuss it if you want :)
