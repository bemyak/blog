+++
title = "Nioh"
date = 2020-11-02
draft = false
[taxonomies]
tags = ["review","bad game"]
+++

A "Souls-like" game that took me 103 hours ~~to complete~~ to drive me to the point where I dropped it.

![nioh cover](https://steamcdn-a.akamaihd.net/steam/apps/485510/library_hero.jpg)

<!-- more -->

Huh, this will be mostly a rant, but don't take my criticism too seriously, I **did** spent quite some time playing it, so it probably worth it.

## "Souls-like"

I'm not a stranger to this genre. I completed each and every Dark Souls game as well as Sekiro. Furthermore, I enjoyed them a lot, except **Dark Souls II**. Nioh has so many in common with it, but it manages to spoil even the goods parts of it. But let's leave that aside for now, the point is that I do have experience with "difficult" games.

**Souls-like** is a greatly overused term. Nowadays, every game that requires a bit of skill gets this label, even if it has nothing to do with Souls series at all (e.g. [Cuphead](https://store.steampowered.com/app/268910/Cuphead/), [Salt and Sanctuary](https://store.steampowered.com/app/283640/Salt_and_Sanctuary), [Jedi: Fallen Order](https://store.steampowered.com/app/1172380/STAR_WARS_Jedi_Fallen_Order/)
🤷)! In this case, this is more or less applicable though, except a {{ hint(text="Metroidvania", hint='Game genre where despite the world being "open" most areas are not accessible until getting certain abilities.') }} component.

I was recommended this game by a good friend of mine that also enjoys this kind of game. He claimed it to be a "very great" one, so I had **some** expectations.

## Technical problems

So it began with me unable to start the game. After a developer logo, there was... just nothing. Luckily, the game setting is a standalone application, so I was still able to try tuning them. With some configurations, the game did launch, but that was either very low FPS or very low resolution.

In the end I:

1. Disabled embedded video adapter in laptop's BIOS settings
1. Tuned some [NVIDIA settings](https://steamcommunity.com/app/485510/discussions/0/1482109512299912250/#c1482109512300016912)
1. Did something else? I don't remember 😄

Anyway, it worked. Not counting crashes, e.g. a recurring one that happened every time I sheltered a weapon after "Grapple" (backstab).

I know that this is a console-first game, but maaaan, WTF?

## Story

It is barely there. The beginning and the end do make sense, but everything between them — does not. In the first chapter you'll need to do ~6 quests on retrieving a lost sword, sometimes even the same one. The repetitiveness of the game is another issue that I'll cover later, but apart from that most of the quests do not feel important or meaningful.

If you want to know what is going on, you'll have to read through walls of text. Cutscenes are very fragmentary and if you'll watch only them, you will have no idea what is going on. I hate this in games, it's like when watching a movie instead of a cool battle scene you will be presented with just a spelled description of it. WHY?! It's like authors are signing in their incompetence.

> Oooh, but that **is** Souls-like! No?

Yes, in Dark Souls story is also not told to a player. But there it was a sane choice, meaning the game **doesn't even try** to tell it. In Nioh the attempt was made, and it is a failure.

## Locations

This is probably the most notable difference. Instead of a huge "open" world, you get just "maps". They are not connected, and you can navigate between maps using a "global map".

At first, I liked this idea. Developers don't have to think about how to connect different biomes into a single maze, they are creating wholesome and beautiful locations.

This also solves the problem with fast travel being mandatory in the later stages of the game when the map becomes huge and time-consuming to navigate.

However, few new issues are introduced:

1. Players have no idea how they got into the new location, how is it connected with others, i.e. where the hell they are
1. They need to explore every new map from scratch. There are no "Aha, I saw this place from there" moments.
1. No replayability. If a player has completed the map, there is no reason for going back and explore it once again, as opposed to Metroidvania-style maps, where you **need** to go through each location many times to reach the ones that became unlocked.

The first and the second issues are just not addressed. Before a mission, you'll get a brief description of what you should do and that's it.

The third one is addressed, but in the most horrible way that is possible. You'll just get another mission on the same map. And again. And again. Oh, and there are also daily challenges if you want to play on the same maps again and again. Hilariously, but this DOES address the second issue: "Aha, this is the same map, but I need to walk through it in another direction now!" 🤦

## Mechanics

The battle is much faster compared to Dark Souls but is a bit slower compared to Sekiro. Unlike Sekiro, it doesn't require a player's "skill" at all. I have literally beaten it with one combo 🤷

**Body** (a.k.a. health) is a total dump stat here. Almost every enemy will take you down with one or two hits. Improving health will give you so small amount of it that it just doesn't worth it. If you want to play a tough tank, you will need to put every single point into this to keep up with the enemy's damage scaling.

Every stat boosts damage from one or two types of weapons, so it makes sense to max one stat sky-high and improve others only to satisfy item requirements.

I ended up maxing **Strength** to use Odachi and **Heart** to have enough stamina for long combos.

What is interesting is that respecs are almost free. They cost 10,000 of gold which you can easily afford even on low levels. Oh, higher levels it is just one or two enemies killed, so I can imagine that it is possible to build a character for each boss fight.

## Items

Oh, this is a headache. The item drop rate is just insane. The inventory bloats at an overwhelming rate. In the beginning when all the items are more or less the same, so it takes a lot of time to figure out which one is better.

> **Hint**: No one. Everything that drops from regular enemies of bosses is just shit, simply sell it as soon as you can. See the [next section](#online)

Each item has a level, which should roughly match yours. You can level up it by "Soul Matching" it with one of the desired levels, but after the first match the price grows exponentially, so in the end, you will have to leave your favorite thing behind and find a new one.

## Online

Nioh takes an idea of "blood pools" from Dark Souls one step further. Not you can summon the "revenant" of the dead player and fight it to get some {{ hint(text="Amrita", hint='You level up spending it, like "souls" in that other game') }} or some of the equipped items.

Then this last point completely ruins everything that I wrote in the previous section. I don't know what we're developers hoping for, but 100% of items that I was using dropped from these "revenants". Some of them gave me insane amounts of Amrita, enough to level up once or twice. And since revenants are restored pretty quickly, there was a chance to become so much overpowered!

The battle itself is not difficult at all when you get used to controls. Revenants of **100 levels above yours** can still be beaten.

Even if their sword were Soul Matched a lot of times, the counter will become 0 for you, so you can now enhance an item even further. And then somebody will pick it up from your revenant and enhance it again. So, by the end of the game, you'll know the names of players who share the same gear with you.

Mine Odachi was dealing around 1800 DPS, which was enough to kill everything pretty quickly, even the strongest bosses.

## Bosses

Did I mention that the game is too repetitive? The same maps and so on...

Yeah, the same goes for the bosses. You will fight the first boss tens of times. The hardest bosses will be grouped because why not.

Yes, this will be hard, but also boring as hell. The amount of their HP is ridiculous, hundreds of times more than yours. The only way to survive is to find blind spots.

In the end, bosses are not about skill, just patience.

## Conclusion

The game **IS** challenging, which is great. It has some unique features, interesting mechanics, and beautiful locations.

But I have a feeling that at some point development team just decided that the game is too short and they just copy-pasted it a few times.

The same locations, but in a different direction. The same bosses, but with other bosses. And then the same combination of bosses, but in a different order...

At some point (**AFTER** the main story ends, somewhere in DLC) I just abounded it, because I realized that I got all the content that is there.
