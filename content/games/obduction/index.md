+++
title = "Obduction 🏠"
date = 2022-02-15
draft = false
[taxonomies]
tags = ["review","mediocre game"]
+++

{{ img(src="https://steamcdn-a.akamaihd.net/steam/apps/306760/library_hero.jpg", alt="obduction cover") }}

A game from the creators of legendary Myst. Very experienced developers, working in the familiar genre, a new story setting to allow experiments and innovations, very short (14 hours). What could possibly go wrong?

<!-- more -->

To begin with, I got this game for free on a GOG's giveaway. With all the criticism said later, the game surely worth it's money 😄 It would still be, even if the price would be greater than zero, I did enjoy the game after all. However, I wouldn't pay full €30 for it.

> **MINOR SPOILERS AHEAD**. I warned you.

## Preamble 🧙: Myst

I played only [Myst V: End of Ages](https://en.m.wikipedia.org/wiki/Myst_V:_End_of_Ages) and the experience was... Frustrating to say the least. Mom have bought me a glorious huge CD case with 4 installation disks, so my expectations were sky-high. The game didn't explain much, just throw young me into an unknown universe and gave me a plate that I could draw on... and there were portals to some alien dimensions... That's all I remember.

I haven't progressed much on my own, got stuck almost instantly and much later with a help of my future wife and a walkthrough we have completed it.

The game didn't work out for me, but I know a lot of people enjoy this genre. Most of the riddles seemed counterintuitive to me, the story was barely explained and there was almost nothing to find pleasure in, except admiring stunning environments.

11 years forward the Obduction is released.

## Story 📚: +1

It's excellent. The player arrives into a new world which laws they need to explore themselves. The laws are complex but awesome. They are introduced very gradually, one at a time:

1. Oh, I'm on a piece of the Earth embedded into some alien planet? Cool!
2. Ooh, the spherical border of this piece teleports you to the opposite point in the sphere!
3. Oooh, there are other dimensions! Wow!
...

And it all makes sense in the end. Every piece of puzzle takes its place, and you can see the whole picture right before the credits.

The only problem is how this story is communicated to the player. It starts with a cool linear intro with a dialog playing on the background talking about what's going on right now. But then the most of it are just notes, books, obscure clues and very, VERY poor dialogs with a couple of NPCs. You can easily miss a part of the plot, and later you will be wondering what the heck is going on.

This happened to me, so I just ended up reading the plot on Wikipedia. I didn't pay much attention to the walls of text, so I had to pay for it 😅

As said, the interactions with NPCs are pure cringe. It's like 12-yo was trying to make a movie with a stolen dad's camera. The quality is fine (dad bought a decent camera!), but the actor play and dialogs leave much to be desired.

## Inventory 🎒🙅: +2

Let's start with positive!

The inventory is absent! Completely! And this is truly awesome! Why? It removes most of the frustration that was present in Myst.

See, if you have inventory, you will have items in it. Those items can be combined with each other and with objects in a world. If you have 10 items and there are 10 interactable objects, the player ends up with 145 possible combinations! Some of them make sense to the player, some did to developers. Are these sets intersected? Probably not fully, thus frustration arises. So, **inventory is bad**!

With no inventory all what's left are objects in the world. Their state can be changed, some of them can be moved, but this is much easier to control. All puzzles now have clear bounds, no need to brute force you way through the game anymore.

Thanks to this, I was able to solve all the puzzles without any hustle.

## Details ⚙️: +1

Wires, gears, locks, traces, constructions, materials, mechanisms: such things are extremely important for such games. If you're wondering why is there no electricity, you will be looking for wire and chech where it goes. If you can't open a door, you'll check for how is it locked, blocked or barricade. And this is all in place here, with nice color codes that are easy to read. Just pay enough attention and any puzzle will be solved!

## Alien numbers 💜: +2

I won't spoil too much, but the developers managed to come up with a very beautiful and unique alien number system. It's not used much and most puzzle that involve them can be brute-forced, but it is there! Truly a gem 💎

## Frustrating intro 🤔: -1

Time for the things that are not so great!

The first ~30-60 min of the game all you will be seeing are just locked doors, mechanisms that you can't interact with just yet, buttons that do not work and so on. You must do certain things in certain order to get some freedom, but until that is done, you're in a tiny labyrinth with only one way through.

This experience helps later: when you'll have more options you will already know where to apply them, but the initial part is still poor.

## Length ⏳: -2

The length itself is not a big issue, but the measures developers took to extend it are dreadful. You will be constantly backtracking. Once I had to cross the map **7 times** to solve a puzzle and unlock a shortcut.

Imagine a map like this:

{% plantuml() %}
skinparam componentStyle rectangle
[A] -> [B]
[B] -> [C]
[A] .> [C]
[C] -> [D]
[D] -> [E]
[C] .> [E]
[E] -> [F]
{% end %}

The way shortcuts work in other games is pretty straightforward: once you've cleared some distant zone (`C` or `E` on the diagram), a shortcut is unlocked. Usually shortcuts are chained, so you can quickly get from `A` to `E`.

As you might have guessed already, this is not the way shortcuts work in Obduction. In order to unlock `A -> C` you will need to get somewhere near `E` 🤷.

Even worse is that by the time you get to `E` you will need to go neither to `B` nor to `C` nor to `D`, even though before you had to track all the way from `A` to `E` many, **MANY** times 😭.

There are a lot of puzzles that teleport you to another dimension. The full interaction time with a machine takes **30 seconds**. And there is a very cool animation, so it is not a big issue, right? Wrong! You'll get tired of this animation on the 5th teleportation, but some puzzles require you to perform this trick ~20 times 😱

> Out of 14 hours I've spent ~50 mins just waiting for animation and loading to complete 😭

The puzzles themselves are not difficult at all, they require only time.

## Conclusion: -1

Very nice game, some great ideas, but probably due to lack of budget the amount of content is disappointingly limited.
