+++
title = "Blasphemous"
date = 2022-01-16
draft = false
[taxonomies]
tags = ["review","good game"]
+++

![Blasphemous cover](https://steamcdn-a.akamaihd.net/steam/apps/774361/library_hero.jpg)

A really quick note on the [Blasphemous](https://store.steampowered.com/app/774361/) game. TL;DR: It's a good one!

<!-- more -->

Blasphemous is a classic metroidvania inspired by middle-ages Spanish lore. The (pixel) artwork is gorgeous, the story is OK, the gameplay "clicks". The most enjoyable part is exploration: the world is vast, and you never know what awaits you in the next room.

Some art brings up a feeling of disgust and awe mixed together. It's hard to describe, but it is on that side of a gore that still doesn't make you feel sick, but it's not far away from that line.

The bosses are cool, very well done, with good telegraphing of attacks that I was able to capture on the first try. All the "basic" ones are pretty simple, I beat them from the first of second try. However, if you want CHALLENGE, you will get plenty on it on New Game +. The DLC bosses are especially challenging yet interesting.

Mechanically, it's still a good old metroidvania and platformer. One thing that stands out is that you don't lose _tears_ (a.k.a. souls) on death, you just have you magic reduced.  You can have multiple _guilt fragments_ (a.k.a. blood spots) lying around, so death is not punishing at all.

A very-very cool idea that in my opinion could play a lot better is **relics**. They are artifacts that the character can equip that _changes your perception of the world_. Sounds cool? Nah, in practice it just revels messages and platforms. However, the idea is really intriguing. What if they would really change your perception?

If you are waiting for the [Silksong](https://store.steampowered.com/app/1030300) like me, you definitely should give a try.
