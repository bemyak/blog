+++
title = "Mortal Shell"
date = 2022-01-30
[taxonomies]
tags = ["bad game", "review"]
+++

{{ img(src="https://steamcdn-a.akamaihd.net/steam/apps/1110910/library_hero.jpg", alt="Mortal Shell cover") }}

The game I was playing for 25 hours while waiting for the Elden Ring.
It was quite an experience!

<!-- more -->

> Minor spoilers ahead.

## ➕ Graphics

The visuals are so ahead of Dark Souls' and Elden Ring's that you keep wondering why can't From Software hire a bunch of good devs and make it the right way already!

## ➕ Shells

Instead of classes, we have different "shells" (dead bodies) that The Foundling can wear.
Each shell is levelled up independently and has different stats and abilities.

I like how this plays out. You can have two shells up your sleeve when the situation is right.

The most fantastic feature is the "death mechanic".
When you're out of health, the Foundling got kicked out of a shell and can still fight or reclaim the lost shell.
This ability restore after resting or upon defeating enough enemies.

## 〰️ Hardening

Hardening replaced all blocking abilities in the game.
You become invulnerable but motionless as long you want.

It looks and feels great, but the impact on the gameplay is severe.
It breaks the flow of the combat, turning battles into a patience game rather than a dance.
And it's a bit annoying when bosses use this mechanic.

The winning strategy for all the fights is to make a heavy jumping attack and harden the moment before a weapon hits the ground.
This way, the enemy will first take a hit from just touching your blade and then from the ground slam when you unfreeze.
Repeat until you win.

## ➖ Game progress

The game world is pretty concise. There are:

1. A tutorial area which you can't come back to
1. The central hub — a huge starting location full of enemies which connects to all other locations
1. A small and easy dungeon
1. A medium dungeon
1. A large and hard dungeon

The route seems evident on paper, but no one tells you this in-game.
You don't know the size of a dungeon when you enter one, so it's unlikely that you'll go from the easiest to the hardest.

Near every dungeon lies a shell. Inside every dungeon is a weapon. Outside is a chest that opens only after beating a dungeon boss. The chest contains an item that allows you to summon that weapon at will.

Again, pretty obvious design, one might say, right?
In practice, this doesn't work.
I first explored the hub area and found all shells.
Of course, I didn't recall what shell was near a dungeon when I entered it, so I used only one most of the time.

After realizing a weapon was at every entrance, I collected all weapons immediately.
Then after completing the first dungeon and seeing that the exit chest was open, I just collected all the switch weapon items.

I _think_ this is not how it is intended, but after only one boss, I had (almost) all shells and weapons in the game :shrug:

## ➖ Locations

The hub location is mostly fine, except that it is enormous and full of enemies, and you must traverse it tens of times since it's a hub.

The small and medium dungeons are challenging but fun.

The largest one is a complete disaster. It's just huge for no good reason.
Not very hard, not very interesting, just incredibly dull.

## ➖ Bosses

There is a boss in every dungeon and the final boss.
They are OK and took me a couple of tries.
Each has some unique look, move set and mechanics.

However, there are also:

1. A tutorial boss
1. A boss for each of the four weapons
1. A DLC boss that gives a new shell

… and **ALL** of these bosses are the same NPC.
This is just ridiculous.
You fight the same guy **SIX** times more than there are other bosses in the game.
The fact that they've added one more encounter with him in a paid DLC is absurd.

## Conclusion

The game features a couple of interesting mechanics, like shells and hardening, but it fails hard in everything else.

I hope the devs will learn from their mistakes, and then we can see something truly remarkable.
