+++
title = "STAR WARS Jedi: Fallen Order™"
date = 2020-12-10
draft = false
[taxonomies]
tags = ["review","good game"]
+++

![jedi cover](https://steamcdn-a.akamaihd.net/steam/apps/1172380/library_hero.jpg)

A pretty short game: 33 hours for 100% completion, but has a lot of fun in it! Let's keep this review short as well :)

I'll skip the praising part and be right to the critics. But please, **do read the conclusion**

<!-- more -->

## World

Welcome to the Star Wars universe!

The game consists basically out of four "planets" that you can explore like in any open-world game. Sounds fun, right? Well, I lied, you can't.

You see, this is a **metroidvania** type of game, so most areas can't be accessed right away. How many? Well almost all of them.

Despite being an "open-world" you are forced to explore areas in a strict order determined by the plot. Got to the wrong planet? Too bad for you, you'll get your ass kicked for half an hour only to find a gap that you cannot cross. Okay, there is a nice upgrade there as well, but you will still feel tricked.

> "Open World" is a lie

Did I mention that this a metroidvania? Yes, you get a lot of fun skills that allow you to access new areas. Wall running, how cool is that? Well, no so cool since you can run only on very specific walls that developers place there for you.

This is the same thing for which I hate Portal 2: developers give you awesome abilities, but then restrict you, so you can't play with them and have fun. In portal 2 you were allowed to place portals only on _specific sections of the wall_. Here you can use your abilities only in certain places with certain items and solve puzzles only the way they meant to be solved. So bad.

Out of four planets, only one is gigantic: Zeffo. The others look just like small plot stabs with new abilities and items to collect and come back when you have more abilities to collect more items...

Speaking of items, there are 3 types of those:

* Sword upgrades (cosmetic)
* Ship upgrade (cosmetic)
* Ponchos (you get it)

And none of them is good, except some sword skins, maybe. I just played the whole game equipped in some stuff from DLC, which was the only more or less decent one.

The level design is good! When you play a level the first time. After that navigation becomes really difficult. In {{ hint(text="one area", hint="the one with the bird") }} you are literally forbidden to go back for no reason. The camera just doesn't turn that way. Open world, huh?

The last thing here is Zombies. Seriously, what the fuck? Zombies in Star Wars? Created literally by necromancy? Like, with MAGICAL SPELLS. Ah, probably I just don't get it.

## RPG mechanics

Ah, well, it is _kind of_ there.
You do get ~~souls~~ experience points, and you can spend them to buy some skills. But you can't min-max and invest in one branch because you're locked to a certain set of upgrades until you make progress in the main quest. Together with the high amount of XP you get, you can't have something **not** upgraded, which doesn't make any sense.

## Story

It is there and it is fine. To my taste it had some flaws:

* Cal gets really close with his companions in no time. Yes, they saved his life, but he speaks of them like about the closest friends in like 30 in-game minutes he met them
* On the other side, when he takes offense from Cere he bugs everyone for many many gameplay hours

Otherwise, the plot is rather simple, but the ending is something to remember.

## Combat

Again, it is there, but that's it. You can do cool combos with a sword, but most of them consume "force". The **only** ways to restore it are killing enemies and resting. This means that you will always save it for later, expecting tougher enemies and encounters.

The pool is also pretty small: in the begging, it is barely enough to make two or three "strong" attacks, so I ended up getting used to living without them. The solution could be pretty simple: restore it outside the combat. This could still make the players manage it carefully, but won't prevent avoiding it completely.

There is some skill that you should get to master the combat and beat enemies easily, but it is not a great curve, you just "get it" at some point without too much trouble.

## Bugs

{{ img(src="lightsaber.jpg", comment="What could go wrong if you forgot to turn off your lightsaber?") }}

Not cool space bugs, just basic programming mistakes. A lot of visual glitches, walls and floors clipping, T-shaped poses, and so on.

The most annoying thing is camera behavior in corners. Sometimes enemies sit tight on you and push you back to the wall. Since the game uses a third-person, guess where it ends up? Right, just between Cal and the wall.

## Minor conclusion

Time for a reveal: this is a Harry Potter game (like the first or the second one). You jump on ~~mushrooms~~ **space** mushrooms, run away from ~~troll~~ enemy spaceship down the ~~corridor~~ muddy hill, fight with ~~magical monsters~~ fantastical creatures and collect ~~cards~~ useless items.

What else is in there? Just a bit of **everything**.

"RPG" in an "Open World" with elements of "metroidvania" and "Souls-Like" fighting and bosses. They just added whatever they could without any story or gameplay reasons.

## Major conclusion

... and that worked out. Despite EVERYTHING listed above, the game **IS** fun. You don't notice most of the flaws during gameplay, only in retrospective.

You always have a clear goal, so the amount of frustration is minimal. Furthermore, you get your ass beaten from time to time, so the difficulty is OK and there are some things that you as a player can master.

The game is a real diamond build from dirt.

And this is amazing.
