+++
title = "Mafia: Definitive Edition"
date = 2020-11-20
draft = false
[taxonomies]
tags = ["review","good game"]
+++

{{ img(src="https://steamcdn-a.akamaihd.net/steam/apps/1030840/library_hero.jpg", alt="elex cover") }}

Less than 40 hours to get 100% of achievements. A very short, nostalgic reincarnation of a childhood game. And a guide on how to win the race ;)

<!-- more -->

I got the first version of Mafia when I was around 11 years old. My parents bought it to me literally on a pirate software market, and the seller said that it's like GTA: III, but you can shoot through the windshield! Amazing, all the ads I needed that time.

Turned out it was not the only difference back then. Compared to GTA: III, Mafia felt a lot... calmer? relaxed? slower maybe? Instead of a city that you just want to destroy, here was the city that I enjoyed driving around. The characters were deep and felt real for some reason. The missions were hard but remarkable.

And now comes the remake.

## Comparing old and new

You can't play in a remake and not compare it to the original. I remember this city, these characters, these missions, but... I'm different now. My memories are the memories of an 11-12 years old boy, who sneaked out to play Mafia and was amazed by everything in it. My new impressions are impressions of an almost 30 years old man, who played in a lot of stuff, tried to develop some stuff himself, knows a thing or two about programming, computer graphics, plots, and so on.

The point is that these memories are completely different, and we shouldn't compare them. If I'm to play Mafia: I again, I'll get a completely different impression about it, for sure. Definitive Edition is created now, in modern reality with a lot of game design experience behind.

That said, let's be fair and try to judge the game as it is, independent of its predecessor. I'll still reference it, but not too much.

## Story

The story is the reason the game is loved, mostly. You start as a simple cab driver, with a strong moral compass, but step by step, very gradually you become one of the most feared "family members" out there. This transformation is justified, and you feel that you'd probably do the same in given circumstances. This brings a player so closer to the protagonist, that I literally couldn't play in Mafia: II, where the guy starts by doing weird and illogical things.

In the remake, they didn't change much. Small bits and pieces here and there. Overall the story feels smoother, you're less distracted by side missions since they are in the {{ hint(text="Free Ride", hint="just ride around the city, on any car with any weapons, no limits") }} mode.

On the other hand, the "relaxed" mood that I mentioned above is almost lost. You don't cruise so much around the city during the main game anymore, instead, you're jumping from one skirmish into another. Is it that bad? I guess no, not today. You stay focused on the main story, and you can just absorb the content without disruption. This is what older me prefer.

The actor play is excellent and makes the game feel much more alive. The actors fit characters quite well, I enjoyed this new level of detail.

## Mechanics

I think only the _cover_ mechanics were added, which is in its place. Skirmishes became a bit simpler I'd say, though I still had to load quite a few times.

The _driving_ was changed drastically. In the original, it didn't gain enough momentum before the bridge, most cars just couldn't make it up. Now this issue persists only for {{ hint(text="one car", hint="Ace Bolt, yeah") }}, the others are more than fine.

Turns are now more like in Need For Speed. Handling is a total mess, and you got in a drift at almost any turn. Sine cars became a little more powerful, this is an issue and takes some time to get accustomed to.

The problem here is not handling itself, but a ruined "relaxed" experience. It's difficult to cruise around the city when every turn in feels like it was taken out of some cheap racing movie. Multiply this by 10 if there is rain out there.

I don't know why is this implemented the way it is, maybe there are reasons for that. Like, back then the tires were bald, the breaks were bad, and so on. If so, then this is Okay for me, but I doubt that.

## The Race

Speaking of driving, THE RACE is still there. And it is still as difficult as it was, though the preceding mission is much easier. And there is no way {{ hint(text="to cheat", hint='There was "a feature" there, so if you press "respawn" in a certain area, you will get teleported to the start with increased lap counter') }} now, but you can change the difficulty in settings. This is not the way!

If you dare to complete it yourself, here is some advice that will greatly help you.

### Use gamepad

This was a problem in the original too but in a bit of different fashion. To drive correctly, you need to control your front wheels very carefully. In the original, if you were to play with a keyboard, pressing 🠔 or 🠖 will turn them into the most left of right position instantly. Not only does this make handling at high speeds a total mess, but it also slows you down a lot with every press.

Here the situation is a bit better, but the gamepad will still give you more control.

### Use gamepad the right way

The idea is that putting your pad into the extreme position will slow the car down, just as in the previous game incarnation. _"Pushing it very gently"_ doesn't work because, first, there is a dead-zone still, and, second, it requires a lot of self-control to do it carefully when you're stressed in the middle of the race.

Here is the solution:

{{ img(src="pad.svg", alt="How to use your gamepad") }}

You just need to push your pad forward, and then you can move it around suuuper precisely. I now use this technique in every racing game, and it is really helpful.

### Total stop before the turn is fine

Sand, grass, and ground are terrible for handling. Crashing into a wall is also should be avoided most of the time since the damaged car handles badly as well. So, a complete stop before a turn is totally fine, you don't lose almost any with it, trust me. If you'll stop trying to pass the turn at a max speed, you will be at this race much faster.

### Riding on the grass after the first turn

{{ img(src="race_map.jpg", alt="race map") }}

On the other hand, straight or slightly curved trajectories on grass work good. After the first turn until the second you can close the distance to other racers considerably.

### The cars in front of you will blow up

There are four cars in total that will crash:

* One that you "fixed" in the previous mission
* 1st lap, 2nd turn
* 2nd lap, 4th turn
* 3rd lap, 5th turn

It will always be a car in front of you, so don't stress too much if you ain't first, just keep in some distance and wait.

### Watch your tires

Ever wondered why you sometimes have a perfect start, and you're the ~6th before the first turn, and sometimes you can't outrun even the 14th? You need to know how friction works!

When you just press the "gas" your wheels start rotating extremely fast. In most cases, it results in a skid, and you can see these traces after your car:

{{ img(src="start.png", comment="notice the white smoke and black traces after your wheels") }}

While skidding, you lose some momentum, so it should be avoided. There is a chance for each wheel that the skid will stop which improves over time, so eventually, you'll start driving normally. When this happens early — you're among the leaders.

To exit a skid just release the gas and press it again. You won't lose any momentum, so now you'll always have "lucky starts"!

### Break gently

The last piece of advice: If you notice that you pressed break too hard and your car starts drifting, start spamming the break. I used this technique in real life while driving my VAZ-2106, it works =)

## Conclusion

The game is fun. It is not long, which a huge benefit for older me. It is interesting and keeps you involved. There are collectables if you're {{ hint(text="that type of person", hint="I am, unfortunately") }}. The "extra" missions in Free Ride are {{ hint(text="extremely fun", hint="especially the one with UFO!") }}!

I'd wish public transport was implemented, like in original, e.g. trains, trams, but they are not needed. It would be so enjoyable just to sit on a train and make a few circles around the city. Alas!

The "taxi" missions should've been longer as well, so the player could admire the city more.

Otherwise, this is the best remake I've ever played! Thanks, Hangar 13 folks!
