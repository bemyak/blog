+++
title = "Invincible"
date = 2024-02-09
[taxonomies]
tags = ["bad-game", "review"]
+++

![invincible cover](https://steamcdn-a.akamaihd.net/steam/apps/731040/library_hero.jpg)

Invincible is a quite hyped "walking simulator" based on Stanisław Lem's novel.

<!-- more -->

## Positives

On the positive side we have:
* graphics[^1]: the scenery is stunning!
* style: retro futurism in all its beauty
* overall story and setting

[^1] if you don't look too closely at visual glitches, holes in the terrain, not fully loaded textures, etc

## Negatives

The negative side will be slightly bigger :)

### Wrong expectations

The game starts with you picking up your equipment from the ground, checking every pocket of your backpack, determining your position based on landmarks. I absolutely loved the first half an hour of the game, when it feels like a first-person survival on an alien planet, but with a cool plot. Will you jump from a high cliff or take your time and look for a slope? Will you take your broken metal detector with you? 

However, the game just drops the ball after the tutorial. The rest of the game is just walking and talking. Most of the small decisions you made don't affect anything. Speaking of...

### Bad decision system (sometimes)

The most crucial decision at the end of the game is well explaining. The characters go through everything once again, so you can make you mind about how the story should end.

However, smaller (but still important) decisions are now like this. In one case a life on a teammate depends on weather you go left or right, and the game doesn't tell which path is the correct one. Distilled frustration!

### Pace

I started wishing for a "run" button after 5 minutes in the game. I found it quickly (even though it's not listed in the controls), but things didn't improve much: the movement is still unbearably slow.

That would have work out fine in the levels would be denser, but here we have huge almost empty locations. Again, that could have worked (we're on an uninhabitable planet after all), but once in a while there's still _something_ at the edge of a map, so the game rewards (aka pushes to) exploring. Which is boring as hell.

### Invisible walls

Imagine, you're in a cave and there are three exists out of it. No clear indication where goes which. You choose one, pick out of the corner and decide "Nah, I'll try another". But you can't! There's an invisible wall blocking you way. Even though all three passages lead to the same place, that's not a reason to cause player discomfort! And this happens for the entirety of the play through.

### Useless tools

You're a space scientist, you're well equipped! You have your journal, spyglass, metal detector and radar. Each has a hotkey and can be used at any time. Sounds amazing, right? Not really, since you'll need them only a few times when the game explicitly tells you so.

### Story

Let's be real, the whole plot theme is pretty much outdated. The Talos Principle and The Witness rose somewhat similar topics, but they dug dipper, asked questions more wisely and showed all sides of the dilemmas more even.

Today we're dealing with machines which pass Turing's test  for breakfast and the only honest answer to the question "is it sentient" is "depends on how you define sentience".

There was no moral dilemma waiting for me here, no reevaluation of my assumptions or views. It's just a straightforward story with a button in the end. We've been here before.

## Conclusion

Boredom and frustration spiced with stunning visuals. That's it.
