+++
title = "The Witness"
date = 2020-07-20
draft = false
[taxonomies]
tags = ["review","good game"]
+++

![witness cover](https://steamcdn-a.akamaihd.net/steam/apps/210970/library_hero.jpg)

A great puzzle game! Took me 55 enjoyable (mostly) hours in beat.

<!-- more -->

> **MINOR SPOILERS AHEAD**. I warned you.

Before you start playing, probably you need to know an important thing: this game was developed by **Jonathan Blow**. So what, you'll ask?

He's a great man, but he has some strong opinions on how games should _feel_ like, how that player should discover them, their purpose, and so on.

Take look at these videos, they are **really** interesting, whether you agree with Jonathan or not (they are quite long though, so maybe add them to bookmarks?):

{{ youtube(id="UwBl7Rnkt78", class="youtube", comment="On games and game design") }}

{{ youtube(id="pW-SOdj4Kkk", class="youtube", comment="On general state of IT") }}

{{ youtube(id="YdSdvIRkkDY", class="youtube", comment="The Witness documentary") }}

To summarize, he states the following:

* Games are played because the player wants to play, thus you don't really need any additional motivation
* Games don't need any tutorials if they are properly designed. The whole game **is** a tutorial
* Games are just games. Yes, that's simple.

And you will feel and the benefits and downsides of these bullets in the game. And for me, there were much more benefits!

## Setting and visuals

The game is happening on a small island in the middle of nowhere. The island is separated into several zones, each with different types of puzzles, and the City with all the types. There also will be _A Final Chamber_, with all of them in hard difficulty, but I'll stop at this point not to spoil too much.

The colors are **SUPER** bright, at some point, I wanted to lower my screen brightness, but there are not so many such places. The meshes are very simple, the textures are almost plain colors with some normal maps. This looks very consistent, simple, and elegant. You don't feel like you want "better graphics" of something. It is just great and beautiful and will remain so for maaaany years.

> **MAJOR SPOILERS AHEAD**. I warned you. Just skip the rest of the section.

Speaking of the island, there is a lake in the middle of it. And it is brilliant:

> The lake is a map of the island.\
> Each lightbox that is lit represents an activated laser.\
> Each fountain that shoots upward represents a completed obelisk.\
> Each open clam represents a completed video location puzzle.\
> Each leaf represents the location of a discarded triangle panel.\
> The leaf turns darker once you've completed it.\
> Flowers represent audio log locations.\
> Bloomed flowers show you've found it.\
> And if they're yellow, it means they are underground.\
> The statue of the woman is the mountain and becomes raised once you've "completed" the game.\
> \
> _© [eys](https://steamcommunity.com/profiles/76561198036156656) from [Steam Discussion](https://steamcommunity.com/app/210970/discussions/4/366298942100178024)_

## Tutorial?

There is none, but you don't really want or need any. Everything starts with simple self-obvious things, and they are getting more and more complicated step-by-step, letting you learn the language of the game just like kids are learning the natural language. And this feels great, rewarding, and you'll feel veeery smart!

At some point you just understand that "Okay, I can't solve this right now, I need to know more rules on how it works". You just go to a different part of an island or walk around this one. You know there **will be something** that will help you.

There are things that the game intentionally doesn't explain to you, but that content is optional and for "smart asses". You will probably find some of it nevertheless, and this will open a whole new level of the game. But I'm running ahead.

The point here is that The game design is almost perfect. That's it.

## Puzzles

I already said a lot about them before, so I won't repeat myself here. One thing you **really** need to know:

> There **are** f\*\*\*\*\*g sound puzzles

Personally, I can't tell the difference between a high note and a low one, so I spent tens of minutes on the easiest of those puzzles. When this became difficult, well... I just gave up. This was one of three times when I referred to a walkthrough. I know some folks on the internet were taking spectrograms of the sound, but that's a bit too much for me.

**Environment puzzles are great**. The moment when I discovered the first one was probably the greatest in my gaming experiences.

## Story

> **MAJOR SPOILERS AHEAD**. I warned you. Just skip this section.

There is none. You just awake in a tube and start exploring the island. You'll find some audio records, that could potentially give you some lore, but instead, they just give you some interesting quotes to chew in your head while continuing solving puzzles. There are black obelisks that become white when you find all the environment puzzles around, but this gives you nothing. The whole game is pointless and the _secret ending_ just lets you know this.

But does it really need a plot? I don't know, but for some reason, I felt a _bit disappointed_. That is because of some false expectations, that I shouldn't probably have, but still.

Oh, the f\*\*\*\*\*g **CHALLENGE**. This is just a nightmare, which it is supposed to be. I have no idea how do people beat it without a [glitch](https://www.youtube.com/watch?v=IqBFrC3xuUc), which is still **quite challenging**.

## Competitors

Time to do some comparison! I played two games that felt somewhat similar: **The Talos Principle** and **Myst**. Both have some pros and cons compared to the subject, so let's briefly go through them.

### The Talos Principle

This is the closest one. You also play as a nameless _thing_ and sole puzzles. I'd say that puzzles in The Witness are superior, but the lack of the story plays its part. The Talos Principle was built around old philosophical questions like _What is the conscience?_, _What makes a human?_, and so on. This is the plot, the reason for learning, the thing to think about while playing and the final decision to make. This makes things emotional and gives sense to a computer game.

The Witness lacks it all. There is a clear statement: _The game is a game. Nothing more._ The game plays with the gamer, while the gamer plays the game. There is some elegance in this, but still, the _feeling_ after beating the game is... Well, I said that before.

### Myst

The Myst is such a classic example of puzzle games, that it could be called the ancestor of them all. It created this genre, but it is not the best representative. It has everything that people hate about puzzles: obfuscation and... well, that is enough :)

The Witness managed to surpass it, every puzzle can be solved with a little thinking. You don't need to randomly apply random items to random items hoping to move the game further just a bit. You even have items to be applied, just great puzzles. So, no jamming, the game process is _extremely smooth_.

On the other hand, Myst offers you a world with a lot of, well, mysteries. There is something **magical** and wonderful in it. The Witness's world feels... plain and boring, maybe? You don't really feel this while playing though, environment puzzles and the shadow play accompanied by the bright colors constantly give you this **WOW** effect. Maybe I just like magic and mystery too much, but Myst's world is just more interesting, it contains more stories to tell and more impressions to give.

## Conclusion?

Jonathan Blow did a great job. In his way. The game is really enjoyable, just don't try to look for the deep philosophical meaning in this.

Points to carry out of this:

* If your game has or should have a tutorial, something is wrong with it.
* You don't need motivators to keep a player motivated. Just do not ruin the mood.
* Let the player decide on what difficulty to play by introducing in-game choices and optional hi-challenging areas.
* Keep in mind that there always will be crazy completionists (like myself), who needs to **HAVE IT ALL**. Give them something to play with, but don't make it like a stone wall.

## P.S.

Here is the last video about the game creation. Jonathan talking about a lot of stuff in there, design choices, challenges, and compromises. Fascinating one, but **FULL** of spoilers:

{{ youtube(id="jhEDARvLf90") }}
