+++
title = "Elex"
date = 2020-07-15
draft = false
[taxonomies]
tags = ["review","good game"]
+++

![Elex cover](https://steamcdn-a.akamaihd.net/steam/apps/411300/library_hero.jpg)

So, 99 hours and I've completed Elex! It was one of a very few games that I was really sad to turn off the last time. It is in no sense a perfect game, but still a really enjoyable one. But let's start from the beginning.

<!-- more -->

> **MINOR SPOILERS AHEAD**. I warned you.

In short, Elex is an RPG game, which takes you to the planet of Magalan - a post-apocalyptic world that strives for survival. There are 4 major factions, each with its own point of view on what that survival should look like.

> Even shorter: The Gothic in the Fallout world.

Before I start splitting the love all over this post, some hate goes first:

* Some bugs are there. The game has seen only one patch, which is incredible nowadays, but it still lacks some polishing. Nothing critical that breaks the game or prevents the walkthrough though. The mentioned patch breaks night illumination btw, so I highly recommend [installing mod](https://www.nexusmods.com/elex/mods/55), that fixes it, otherwise, you can't even tell if it is a day or a night now.
* Sound... could be better. Much, much better. The main issue is footsteps sound, which is too loud, annoying, and sometimes doesn't match the surface. Music is not great also, but all the dialogues are dubbed and voice acting is decent, IMO, though I heard some complaints.
* Fighting system feels sooo clunky. I was playing with a controller, so used Lock Target a lot. This makes the character run slower, limits the view, and makes combat against several enemies way difficult. On the other hand, not using it is even worse, because not only do you have to rotate a camera constantly, but some strikes will just miss because they require super-accurate positioning.

Anyway, the game is **GREAT**. Why? Here we go!

## Ideal player-character interactions

I distinguish three types of games, separation of player and character-wise.

1. _A character is a player_, just in a different location and with some other backstory. But players are free to make ANY choices they are willing to make. Take any Elder Scrolls game as an example. Your character is nobody, and **you** decide who it will become. All its choices are your choices in fact. It has no free will.
1. _A player is a spectator_. This is the opposite, when you play as a very specific character, with a name and tight relations to the world. You control their actions, but often not their decisions. This leads to deeper immersion but leaves a player only as a spectator with very little effect on the story. Most of this game is for consoles, like _Fahrenheit_, _The Last of Us_, _God of War_, _Legacy of Kain_, but there are many of that kind for a PC as well: _Arx Fatalis_, all the quests, all the "linear" games.
1. _Something in between_. Well, that's a whole spectrum, indeed :) There are good examples (_Divinity_) and bad examples of how different games approach it.

But the point here is that Elex hit the spot. You start as #2 from this list: A very specific guy, with a very specific mission, story, ambitions, needs, desires, and knowledge of the world, that you, as a player, do not possess. But, as you play, your decisions affect him, and he becomes more like you wanted him to be. Want to be an asshole? Feel free to do become one. Want to help everyone because of a high morality? Go on! And these small choices _really_ matters.

## Interesting dialogues

I try to immerse myself into every game I play, know its world better, but at some point some of them just become boooring. Nothing happens and matters, so I start skipping most non-major dialogues because there is no point in reading walls of texts if they don't really have any effect.

It is not the case in Elex. Dialogues are pretty short, and they have two purposes:

* Give you an idea whom are you talking too
* Show you how this person is integrated into the world

And this works well! At some point, you start to feel like you _know_ some characters, know how they will react to your actions and what will they say to you.

But what is more important, you don't have an urge to skip the dialogue. Each one of them will give you something interesting, be it a clue or a gossip or fun story about the word.

The game somehow keeps you involved in it by itself, you don't need to force yourself in learning NPC stories, you'll learn them organically.

## Deep world interconnections

And this is the most exciting part. Let me tell you a story from my walkthrough, so you'll have an idea of what this really means:

> There was a guard at the city gates of Goliet, named Drog. He was tired standing on a watch, especially after yesterday's booze with his friend. The head hearts and he knows that only beer from the nearby tavern can make it better.

> And he sees a total newbie called Jax trying to enter the city. He comes up with an idea to ask him to bring the beer, as an opportunity for a newbie to make a friend here.

> The newbie is kind, so he agrees. But when he realizes that Brog is not going to pay, he doesn't like that and Brog becomes his secret enemy.

{{ img(src="drog.jpg", alt="Drog") }}

> While newbie was exploring the city, he found out that one of the guards is missing. It turned out to be a guard with whom Brog got drunk recently. It also turned out that mentioned Brog's friend took some drugs and got dead. Brog is grief about that. One should not mix alcohol and stuff!

> Weeks later, not-so-newbie Jax discovers an island where exiles from Goliet live. One of them is a nice lady, who wants to get back into the safe city, so together they come up with a plan of proving her innocence, but someone should take her place. Who? Remember that "beer incident"? So, the newbie decides that it should be Brog, a corrupted drunken guard, who abuses his power.

> Evidence being faked and Brog leaves the Goliet toward a very dangerous area, far more dangerous, than the island where that pretty lady was confined, while she is taking his place as a guard of the gates.

> Months later Regent Of Clerics ex-alb Commander Jax finds a distant land in the middle of nowhere, filled with a deadly monster. It is where the Brog was sitting. He sees Jax and recognizes that newbie he meet so long ago. He knows that his exile is the did of Jax's hands. And he is really pissed of. He asks for a HUGE amount of money to start a new life, and he is really surprised when Jax is handing a big heavy bag of crystals to him.

> Jax suggests that Brog should join the Clerics, so our ex-drunkard ex-guard ex-exile is heading toward the Hort, to start a new life and bring the word of Calaan to the world.

And none of this is related to the main quest. It is just a couple of side quests and even dialogues without any quest. And all the game feels this way, though I felt like the Goliet was a bit better written and polished.

## Conclusion?

The game doesn't have to be perfect to take a place in your heart. If you are a game designer, please, please, please, spend more time on:

* **The choices that matter**. It doesn't have to be a huge moral dilemma, just some simple things, but let the player fill the effect
* **The world that lives**. It doesn't need to be highly advanced AI with a daily schedule and a point there to feed cats, no! Just think more about _Why it is here?_ _What will it do next?_
* **But remember that it's a game**. A player should not get bored. And most players will go through your game only once, no reason to hide any treasures for later. Just spill it over!
