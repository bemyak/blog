+++
title = "Белоруссия"
date = 2018-04-22
+++

> Этот пост был изначально опубликован у меня в [телеграм-блоге](https://t.me/me_andshe/178) и затем выгружен сюда.
> Это накладывает некоторые ограничения на формат, который больше похож на хронику, нежели на осмысленное повествование.
>
> Далее в таких блоках будет текст, который написал наш попутчик и хороший друг Олег Суетнов. Привет, Олег!

Маршрут примерно такой:

{{ gmap(id="1Kd5MMfTdgGtxJJNG2i_jQd0WG8A", comment="Но так мы, конечно, не поедем, потому что поедем по-другому =)") }}

## Смоленск

{{ img(src="photo_2020-12-05_17-30-00.jpg, photo_2020-12-05_17-30-02.jpg", comment="Следующая остановка - Смоленск") }}

Смоленск, кажется, на 70% состоит из церквей и шиномонтажа. Небольшой цивилизованный центр окружён железными дорогами, автомагистралями, остатками крепостной стены и пересечён Днепром. Из консерватории слышится музыка, а нас сдувает холодный ветер около статуи Оленя :)

{{ img(src="photo_2020-12-05_17-30-04.jpg, photo_2020-12-05_17-30-07.jpg, photo_2020-12-05_17-30-09.jpg, photo_2020-12-05_17-30-11.jpg") }}

Что в Смоленске понравилось — это кафе Упитанный Енот :) \
Вкусный шашлык и коктейль "бодрило" очень зашли после долгой дороги

{{ img(src="photo_2020-12-05_17-30-13.jpg") }}
{{ img(src="photo_2020-12-05_17-30-15.jpg", comment="(да, тут вместо стульев качели)") }}

## Минск

Сейчас добрались до Минска, идём гулять

Виды Белоруссии:

{{ img(src="photo_2020-12-05_17-30-17.jpg, photo_2020-12-05_17-30-19.jpg, photo_2020-12-05_17-30-21.jpg") }}

Так, о чём забыл написать... Во-первых, вот на этом мы катаемся, фото до отправления:
{{ img(src="photo_2020-12-05_17-30-23.jpg") }}

Во-вторых, мы не заметили как пересекли границу :) в какой-то момент просто знаки стали на смешном языке, в на встречной полосе выстроилась пробка из фур

Были робкие надписи, что дорога, мол, платная. Выяснилось, что она действительно платная, но не для членов Таможенного союза, в который входит в т.ч. Россия

Так, сходили в музей валунов

Это, на самом деле, кусок парка, в котором... Лежат валуны!
Всё это представляет собой макет Белоруссии. Камнями выложены реки, некоторые города. Другие города обозначены деревьями и кустами.


{{ img(src="photo_2020-12-05_17-30-26.jpg, photo_2020-12-05_17-30-28.jpg, photo_2020-12-05_17-30-30.jpg, photo_2020-12-05_17-30-32.jpg, photo_2020-12-05_17-30-34.jpg, photo_2020-12-05_17-30-37.jpg") }}

Живём в 3х этажном доме, где всё есть. Небольшая кухня, приветливая хозяйка, уютный номер, WiFi, парковка. Все удобства за 79 рублей =)

{{ img(src="photo_2020-12-05_17-30-39.jpg", comment="Кухня такая") }}

Сходили, посмотрели Библиотеку при свете дня, а то вчера видели её с большой рекламой по всем граням
{{ img(src="photo_2020-12-05_17-30-41.jpg, photo_2020-12-05_17-30-43.jpg, photo_2020-12-05_17-30-45.jpg") }}

Потом к нам прилетел Олег, и я надеюсь, что теперь тут будут и совместные фотки тоже ;)

Съездили погулять по центру Минска, поели в ресторане Бессонница рульку, завёрнутую в бекон и пюрешку.

{{ img(src="photo_2020-12-05_17-30-47.jpg", comment="Это Минский нулевой километр, по периметру написаны расстояния до ближайших городов. До Москвы 700км.") }}

Завтра в программе 2 замка и к ночи будем в Беловежской Пуще
{{ img(src="photo_2020-12-05_17-30-50.jpg, photo_2020-12-05_17-30-52.jpg, photo_2020-12-05_17-30-55.jpg, photo_2020-12-05_17-30-57.jpg", comment="") }}

> Чтобы разбавить этот унылый рассказ, залил несколько фоток города и готов всех испугать собой и номером
{{ img(src="photo_2020-12-05_17-31-00.jpg", comment="Олег") }}

> Что вам эти машинные не расскажут =)
>Минское метро неотличимо от нашего - поезда то тут делают, ветки всего 2 или 3, как в уездном городе. Купил жетончики примерно по 18 наших рублей, давно их не видел (а в Питере ещё есть?)
>
>В городе кажется всего одна центральная улица, ровно под ней наша ветка метро.
>
>Супермаркет заполнен теми же товарами, в целом дешевле Москвы. Ничего эксклюзивного не нашёл =(
>
>Аэропорт очень небольшой, заполнен жадными таксистами и рекламой. Но при этом я спокойно уехал оттуда на маршрутке, что ходит раз в полчаса (представляю такое в условном Внуково, ага)
>
>Насчёт денег - кажется снимать в банкоматах местного белорусбанка или вроде того выгоднее обменников. Ну или тиньков щадит

## Замки

Коротко о том, почему тут нет постов:
{{ img(src="photo_2020-12-05_17-31-02.jpg") }}

Ура, меня выпустили! Вчера мы побывали в 2 замках:

**Мирский** — платный, но прикольный, хорошо отремонтированный. Помимо обычной экспозиции там можно лазить по внутренностям башенок, стен. Много узких лесниц и подземная темница! Отличное место

**Нясвижский** — бесплатный, так же хорошо восстановленный. Расположен посреди огромного парка на острове. Взяли аудиогида, и хорошо погуляли.\
Прото-белорусы любили охоту, балы, и узкие проходы в башни =)\
Полазить просто так уже не дали, но интерьеры хорошо обставлены, очень атмосферно

### Мирский замок

{{ img(src="photo_2020-12-05_17-31-04.jpg, photo_2020-12-05_17-31-06.jpg, photo_2020-12-05_17-31-08.jpg, photo_2020-12-05_17-31-11.jpg, photo_2020-12-05_17-31-12.jpg, photo_2020-12-05_17-31-14.jpg, photo_2020-12-05_17-31-16.jpg", comment="") }}

{{ img(src="photo_2020-12-05_17-31-18.jpg, photo_2020-12-05_17-31-21.jpg, photo_2020-12-05_17-31-23.jpg, photo_2020-12-05_17-31-25.jpg, photo_2020-12-05_17-31-27.jpg, photo_2020-12-05_17-31-30.jpg, photo_2020-12-05_17-31-32.jpg, photo_2020-12-05_17-31-35.jpg, photo_2020-12-05_17-31-37.jpg, photo_2020-12-05_17-31-40.jpg") }}

{{ youtube(id="1TRGEklfxYA")}}

{{ img(src="photo_2020-12-05_17-31-42.jpg, photo_2020-12-05_17-31-44.jpg, photo_2020-12-05_17-31-47.jpg, photo_2020-12-05_17-32-01.jpg, photo_2020-12-05_17-32-05.jpg, photo_2020-12-05_17-32-07.jpg") }}


### Нясвижский замок

{{ img(src="photo_2020-12-05_17-32-09.jpg, photo_2020-12-05_17-32-11.jpg, photo_2020-12-05_17-32-13.jpg, photo_2020-12-05_17-32-16.jpg, photo_2020-12-05_17-32-18.jpg, photo_2020-12-05_17-32-21.jpg, photo_2020-12-05_17-32-32.jpg, photo_2020-12-05_17-32-35.jpg, photo_2020-12-05_17-32-37.jpg, photo_2020-12-05_17-32-39.jpg") }}


{{ img(src="photo_2020-12-05_17-32-41.jpg, photo_2020-12-05_17-32-43.jpg, photo_2020-12-05_17-32-45.jpg") }}

{{ img(src="photo_2020-12-05_17-32-47.jpg, photo_2020-12-05_17-32-49.jpg, photo_2020-12-05_17-32-51.jpg, photo_2020-12-05_17-32-53.jpg, photo_2020-12-05_17-32-55.jpg, photo_2020-12-05_17-32-58.jpg, photo_2020-12-05_17-33-00.jpg, photo_2020-12-05_17-33-02.jpg, photo_2020-12-05_17-33-04.jpg, photo_2020-12-05_17-33-06.jpg") }}

## Беловежская Пуща

После замков доехали до Беловежской пущи и сегодня гуляли тут. Огромный заповедник с указателями в стиле "Усадьба налево через 62 км". Посмотрели вольеры с зубрами, оленями, отсутствующей енотовой собакой, и прочей живностью. Отправились на небольшую прогулку в 6 километров и потом, уставшие, кушали в местном ресторанчике. Ресторанчик, похоже, специализируется на лисичках. С ними готовят всё, даже блины! Очень вкусно =)

{{ img(src="photo_2020-12-05_17-33-08.jpg, photo_2020-12-05_17-33-11.jpg, photo_2020-12-05_17-33-14.jpg, photo_2020-12-05_17-33-16.jpg, photo_2020-12-05_17-33-20.jpg, photo_2020-12-05_17-33-23.jpg, photo_2020-12-05_17-33-25.jpg, photo_2020-12-05_17-33-27.jpg, photo_2020-12-05_17-33-29.jpg, photo_2020-12-05_17-33-32.jpg") }}

{{ img(src="photo_2020-12-05_17-33-35.jpg, photo_2020-12-05_17-33-37.jpg, photo_2020-12-05_17-33-40.jpg, photo_2020-12-05_17-33-41.jpg, photo_2020-12-05_17-33-43.jpg, photo_2020-12-05_17-33-44.jpg, photo_2020-12-05_17-33-46.jpg") }}

{{ img(src="photo_2020-12-05_17-33-48.jpg, photo_2020-12-05_17-33-50.jpg, photo_2020-12-05_17-33-52.jpg, photo_2020-12-05_17-33-55.jpg, photo_2020-12-05_17-33-57.jpg, photo_2020-12-05_17-33-59.jpg, photo_2020-12-05_17-34-01.jpg, photo_2020-12-05_17-34-03.jpg, photo_2020-12-05_17-34-05.jpg, photo_2020-12-05_17-34-07.jpg") }}

{{ img(src="photo_2020-12-05_17-34-09.jpg, photo_2020-12-05_17-34-11.jpg, photo_2020-12-05_17-34-13.jpg, photo_2020-12-05_17-34-16.jpg, photo_2020-12-05_17-34-19.jpg, photo_2020-12-05_17-34-21.jpg, photo_2020-12-05_17-34-24.jpg, photo_2020-12-05_17-34-27.jpg, photo_2020-12-05_17-34-29.jpg, photo_2020-12-05_17-34-31.jpg, photo_2020-12-05_17-34-33.jpg") }}

{{ img(src="photo_2020-12-05_17-34-36.jpg", comment="Вот такая штука спасает вечер, а нам предстоит завтра долгий путь)") }}

## Лида

В планах был переезд в Лиду с заездами в Меловые карьеры и Лидский замок. Что-то неладное мы начали подозревать, когда Марина нашла новость от 2015, что на карьерах усилят охрану, потому что туристам нефиг шататься там, где работают. Сейчас по всем дорогам, ведущим к Карьерам запрещён проезд без пропусков =(

Сделав большой крюк, часам к 6 вечера мы добрались до Лиды. У замка огромная территория, но сам по себе он представляет 2 башни и 4 стены. В одной из башен устроена экспозиция, показывающая археологические находки, реконструкции одежды, доспехов, еды и орудий пыток. Последняя наиболее обширная и необычная, только посмотрите на эти маски! На любого найдётся

{{ img(src="photo_2020-12-05_17-34-38.jpg, photo_2020-12-05_17-34-40.jpg, photo_2020-12-05_17-34-42.jpg, photo_2020-12-05_17-34-44.jpg, photo_2020-12-05_17-34-46.jpg, photo_2020-12-05_17-34-48.jpg, photo_2020-12-05_17-34-50.jpg, photo_2020-12-05_17-34-52.jpg, photo_2020-12-05_17-34-54.jpg, photo_2020-12-05_17-34-56.jpg") }}

{{ img(src="photo_2020-12-05_17-34-59.jpg, photo_2020-12-05_17-35-01.jpg, photo_2020-12-05_17-35-03.jpg, photo_2020-12-05_17-35-06.jpg, photo_2020-12-05_17-35-08.jpg, photo_2020-12-05_17-35-10.jpg", comment="") }}

{{ img(src="photo_2020-12-05_17-35-12.jpg, photo_2020-12-05_17-35-14.jpg, photo_2020-12-05_17-35-16.jpg, photo_2020-12-05_17-35-18.jpg, photo_2020-12-05_17-35-20.jpg, photo_2020-12-05_17-35-23.jpg, photo_2020-12-05_17-35-25.jpg, photo_2020-12-05_17-35-27.jpg") }}

## Новогрудский замок
А, забыл. До этого мы заехали в разрушенный Новогрудский замок. Там буквально осталось полстены и ещё полстены восстановили.

{{ img(src="photo_2020-12-05_17-35-29.jpg, photo_2020-12-05_17-35-32.jpg, photo_2020-12-05_17-35-35.jpg, photo_2020-12-05_17-35-38.jpg, photo_2020-12-05_17-35-41.jpg, photo_2020-12-05_17-35-43.jpg, photo_2020-12-05_17-35-45.jpg, photo_2020-12-05_17-35-48.jpg", comment="") }}

{{ img(src="photo_2020-12-05_17-35-50.jpg, photo_2020-12-05_17-35-52.jpg, photo_2020-12-05_17-35-54.jpg, photo_2020-12-05_17-35-56.jpg", comment="") }}

## Гольшанский замок

Из Лиды мы отправились к Озеру Нароч, по пути заехав в Гольшанский замок (точнее, то, что от него осталось)

{{ img(src="photo_2020-12-05_17-35-58.jpg, photo_2020-12-05_17-36-00.jpg, photo_2020-12-05_17-36-02.jpg, photo_2020-12-05_17-36-04.jpg, photo_2020-12-05_17-36-06.jpg, photo_2020-12-05_17-36-08.jpg", comment="") }}

Руины потихоньку реставрируют. С этим замком связано несколько мистических историй, спасибо Марине, что нам их рассказывала, пока там гуляли

{{ img(src="photo_2020-12-05_17-36-10.jpg, photo_2020-12-05_17-36-12.jpg, photo_2020-12-05_17-36-14.jpg, photo_2020-12-05_17-36-16.jpg, photo_2020-12-05_17-36-18.jpg") }}

## Озеро Нароч

Потом мы добрались до Нарочи. Нас встретил улыбчивый половиной лица полноватый мужчина, который показал нам наше "бунгало", и уговаривал попить самогона перед баней 🤔

Сама "Нарочь" — это туристическая зона. Тут есть 3 больших озёра, окруженных лесами. Кое-где есть жизнь в виде деревень и курортный посёлок. Всё это пытается жить за счёт туристов (которые пока не приехали), но как-то очень неумело. Есть дендропарк, "аптекарский сад" — питомник "лекарственных " растений ну и всё.

Природа — очень красивая, остальное так себе.

{{ img(src="photo_2020-12-05_17-36-20.jpg, photo_2020-12-05_17-36-22.jpg, photo_2020-12-05_17-36-24.jpg, photo_2020-12-05_17-36-26.jpg, photo_2020-12-05_17-36-29.jpg, photo_2020-12-05_17-36-31.jpg, photo_2020-12-05_17-36-34.jpg, photo_2020-12-05_17-36-36.jpg, photo_2020-12-05_17-36-39.jpg, photo_2020-12-05_17-36-42.jpg, photo_2020-12-05_17-36-46.jpg", comment="") }}


{{ img(src="photo_2020-12-05_17-36-48.jpg, photo_2020-12-05_17-36-51.jpg, ", comment="") }}

{{ img(src="photo_2020-12-05_17-36-53.jpg, photo_2020-12-05_17-36-56.jpg, photo_2020-12-05_17-36-59.jpg, photo_2020-12-05_17-37-02.jpg, photo_2020-12-05_17-37-06.jpg, photo_2020-12-05_17-37-08.jpg", comment="") }}

{{ img(src="photo_2020-12-05_17-37-10.jpg", comment="") }}

{{ img(src="photo_2020-12-05_17-37-13.jpg, photo_2020-12-05_17-37-15.jpg, photo_2020-12-05_17-37-18.jpg, photo_2020-12-05_17-37-20.jpg, photo_2020-12-05_17-37-23.jpg, photo_2020-12-05_17-37-25.jpg, photo_2020-12-05_17-37-28.jpg", comment="") }}

## Конец

{{ youtube(id="Q0W4uwJbEyA", comment="Короткий ролик о нашей поездке (light version)")}}

{{ youtube(id="UzuUaFPEapY", comment="Короткий ролик о нашей поездке (hard version)")}}

{{ img(src="photo_2020-12-05_17-37-36.jpg, photo_2020-12-05_17-37-38.jpg", comment="Едем домой!") }}
