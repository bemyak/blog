+++
title = "Итоги 2022"
date = 2022-12-31
draft = false
[taxonomies]
tags = ["итоги"]
+++

Никогда не подводил "итогов года", но настало время попробовать.
Я прошёлся по основным своим проектам, оценил, что было сделано и наметил план развития на следующий год.

<!-- more -->

## Жизнь

Увы, моё мрачное пророчество "будет только хуже" из 2018 года сбылось с неожиданной силой.
Из-за него, в основном, мы и переехали, поэтому сейчас чувствую себя везунчиком.

Очень хочется приехать погостить в Россию, но пока шансы это сделать близки к нулю.
Надеюсь, что самая тёмная ночь перед рассветом, и в новом году всё это безумие закончится.

## Здоровье

Я [начал заниматься боулдерингом](@/finland/bouldering/index.ru.md), чтобы хоть как-то держать себя в форме.
Отличный вид спорта, очень мне подходит.

Между тем, в организме начинает ощущается лёгкая деградация: мелкие инциденты, на которые раньше я бы не обратил внимания, теперь вызывают травмы, заживающие месяцами.
Чтобы потянуть плечо, вывихнуть колено, простудить шею теперь, кажется, даже делать ничего не надо, оно всё само возникает на ровном месте. Надеюсь спорт и правильное питание как-то замедлят этот процесс.

## Финляндия

Я доволен жизнью в Финляндии.
Прошёл медовый месяц, прошёл ностальгический блюз по родине, и вот спустя 3,5 года мне всё ещё тут очень нравится.
Да, не всё идеально, пространство для улучшений большое, но условия максимально близки к тем, которые я считаю идеальными.

У нас наконец появились местные друзья и знакомые, поэтому жить стало веселей и кукухе полегчало.

ОЧЕНЬ нужно учить язык. В 2023 обязуюсь подготовиться к экзамену по языку, и, по возможности, сдать его (как уже сделала Марина).

## Аири

Главный мой проект — Аири — успешно растёт и развивается.
Ребёнок — это трудно, в него нужно инвестировать почти всё свободное время, постоянно быть начеку и днём и ночью.
Но эти инвестиции окупаются с лихвой, если прекратить бежать, окунуться в момент, и просто наслаждаться жизнью.
Количество положительных эмоций, которые она мне приносит, я не испытывал, кажется, с тех пор как сам был ребёнком.

Нам повезло: проблемы, про которые мы так много слышали от родственников и друзей, в нашем случае оказались не такими существенными.
Аири хорошо ест, спит, ведёт себя подобающе, почти не болеет, с удовольствием ходит в детский сад, и ладит с воспитателями.

В следующем году нужно начать ходить на всякие секции и кружки.
Пока на примете только плавание и скалолазание, а там посмотрим.

## Работа

Я перешёл работать в другую компанию, и теперь я чуть ближе к тому, что мне нравится — железякам.
Сейчас тружусь в небольшом стартапе, находящимся на ооочень раннем этапе своего развития ("выживание"), несмотря на преклонный для стартапов возраст в 10 лет.

Из моего опыта, я понял, что работать в больших корпорациях — это сразу для меня твёрдое нет.
Работать в стартапах — это "скорее нет".

В будущем, я буду искать работу либо над {{ hint(text="СПО", hint="Свободное Программное Обеспечение. Обычно развивается на пожертвования от частных лиц и компаний, которые идут на найм разработчиков") }}, либо в "bootstrap-стартрапе" — это когда нет инвесторов, и компания существует только на деньги, которые сама зарабатывает. Подробнее об этом в посте о работе, который я давно обещаю, но никак не допишу, потому что причины.

## Стол

У меня икеевский стол Bekant с регулировкой высоты.
Я начал делать проект микроконтроллера, который бы периодически поднимал и опускал его сам, чтобы я не засиживался в одном положении слишком долго.

Все компоненты у меня есть, но я завяз в изучении микроконтроллера и ассемблера.
Занятие это приятное, но нужно переводить знания в практическую плоскость и выдать какой-то результат.

## Замена Dropbox

Все важные файлы я сейчас храню в дропбоксе, потому что это самое дешёвое облачное хранилище на данный момент.

К сожалению, интерфейс его убог до невозможности. Поэтому я нашёл хитрожопый способ платить ещё меньше за собственный Nextcloud-инстанс, но это требует длительных танцев с бубном, чтобы было надёжно и данные не терялись.

Я сейчас активно работаю именно над этим проектом, если кому будет интересно, поделюсь.

## Этот блог

Тут я писал гораздо меньше, чем хотел бы.
Сейчас каждый пост — это целая эпопея.
Нужно сесть, настроиться, и несколько часов долбить по клаве, чтобы никто не отвлекал.
Времени мало, других проектов много, поэтому мысли для блога просто копятся в виде каких-то обрывочных заметок.

Очень скоро мне должен приехать [телефон](https://www.fxtec.com/pro1x) с нормальной физической клавиатурой, и я смогу писать в бóльшем количестве мест, надеюсь это как-то поможет.

![fxtec pro-1x](https://www.fxtec.com/images/pro1x-landscape.jpg)

## Xelbr.Art

> Мир Пепяки: [xelbr.art](https://xelbr.art)

Сделал для Марины сайт, где она наконец смогла организовать все рисунки и материалы по миру Пепяки.
Это была большая работа, и я доволен результатом. А вот количеством посещений — недоволен :)

Нужно его получше оптимизировать для разных разрешений экранов, слишком всё гигантично выглядит на маленьких устройствах.

В процессе обнаружил несколько багов или недостающих фичей в движке Zola, на котором работают xelbr.art и мой блог, и успешно их исправил.
Zola, на мой вкус, стала пригодна для широкого применения и нетривиальных случаев использования. Очень рад тому, как развивается проект, хотя опасаюсь, что мейнтейнер выгорит с станет сумасбродом.

## Zola-new

![zola-new screenshot](https://gitlab.com/bemyak/zola-new/-/raw/master/screenshot.png)

Сделал небольшую полезную утилиту для Zola, чтобы было приятней создавать новые посты.
Вроде ей даже кто-то пользуется помимо меня, что не может не радовать.

В процессе работы над ней, родился и умер TUI-фреймворк [thuja](https://gitlab.com/bemyak/thuja).
Родился потому что все аналоги омерзительны в использовании, а умер — потому что мне потребовалась фича Rust, которая называется [специализация](https://github.com/rust-lang/rfcs/blob/master/text/1210-impl-specialization.md), а она [ещё не готова](https://github.com/rust-lang/rust/issues/31844).
Да и лениво было тащить большой проект одному, если честно.

## Roll Bot

> Бот для Телеграмма: [@roll_bot](https://t.me/roll_bot)
> ежемесячно получает 10000+ сообщений
> ежемесячно около 400 активных пользователей

Я довёл бота до состояния, когда я им доволен.
В основном, я исправлял баги, но была проведена большая работа над парсером бросков.
До этого, он мог примерно понять что от него хотят, но было очень много граничных случаев, когда парсер ломался, и на это постоянно налетали пользователи.
Они даже создали свод правил, как избегать этих случаев, нащупали лимиты и научились работать в их пределах.
Мне пришлось нехило залезть в теорию, но это совершенно не помогло. Помог неплохой [визуализатор](https://github.com/fasterthanlime/pegviz), который я слегка [допилил](https://github.com/fasterthanlime/pegviz/compare/master...bemyak:pegviz:master).
Теперь нет известных мне случаев падений и ошибок, всё работает как часы.

Есть ещё несколько пожеланий, которые можно было бы реализовать, но они незначительны.
В целом, проект (который я тащил с 2017 года) завершён :tada:

Мне удалось сформировать небольшое сообщество вокруг проекта.
Пользователи собираются в чате, обсуждают баги, радуются обновлениям и просто общаются.
Не слишком регулярно, но всё же.

## Squarifier

> Оквадрачиватель картинок в виде прокси-сервера: [squarifier.ml](https://squarifier.ml/)

Почти не касался этого проекта, но поддерживал в живом и рабочем состоянии. Очень хочу доделать автоматическое вырезание белого фона у изображений — это интересная алгоритмическая задачка, и сделать чуть более человеческий интерфейс.
Может, в новом году...

## ArtCalendar

> Рождественский календарь для художников: [calendar.xelbr.art](https://calendar.xelbr.art/)

Бóльшая часть работы была сделана в 2020, когда календарь запустился первый раз.
В этому году я обновил движок, починил несколько багов.
Всё ещё недоволен тем, как выглядит шрифт, но починю уже в 2023, когда перевезу на Godot 4.

## Pepyaka: The Game

> [Текущая версия](https://bemyak.gitlab.io/pepyaka/)

Не касался нашей игры уже пару лет, но очень хочу довести её до минимально рабочего состояния.
Чтобы уровень можно было пройти, чтобы хоть какой-то геймплей был.

## Мастодон

> [Мой профиль](https://lor.sh/@bemyak)

Довольно активно (по моим меркам) вёл аккаунт в Мастодоне.
Когда-то мне казалось, что это прекрасная, с архитектурной точки зрения, социальная сеть (децентрализация, отсутствие единого владельца).
Но сейчас я всё больше уверяюсь, что масштабируется это решение ужасно, и возлагает кучу ответственности на админа инстанса, который зачастую не знает, во что ввязался.

Подход вроде ZeroNet мне кажется гораздо более интересным: каждый пользователь хранит свои данные и те, которые ему нужны, и раздаёт это всё как торренты другим пользователям.

В новом году планирую почитать побольше про протокол Activity Pub, как работают торренты (DHT) в деталях, влезть в GNUnet и попробовать написать что-то для него.

## Итого

Я практически уверен, что не смогу продолжать в том же темпе, поэтому надо сосредоточится на завершении текущих проектов.

Важное:

- [ ] Выучить финский
- [ ] Переехать с Dropbox
- [ ] Автоматизировать подъём стола
- [ ] Доделать игру

Желательное:

- [ ] Подшлифовать текущие проекты
- [ ] Дальше изучать assembler
- [ ] Поизучать децентрализованные сети, написать MVP для GNUnet
