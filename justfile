alias watch := serve

# Convert all ttf fonts to woff and woff2
gen_fonts:
    #!/usr/bin/env nu
    cd static/fonts
    let files = ls | each {$in.name | path parse} | where extension == "ttf"
    $files | each { ttx -f -o $"($in.stem).woff" $"($in.stem).($in.extension)" }
    $files | each { woff2_compress $"($in.stem).($in.extension)" }
    ls

# Build with nix
nix_build:
    nix build ".?submodules=1#" --option sandbox false --extra-experimental-features "nix-command flakes"

# Build with zola
build:
    zola build

# Run zola and watch for changes
serve:
    zola serve

# Update flake
update:
    nix flake update
