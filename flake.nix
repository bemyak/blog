{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    utils.url = "github:numtide/flake-utils";
    zola-new = {
      url = "gitlab:bemyak/zola-new";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "utils";
    };
  };

  outputs =
    {
      self,
      nixpkgs,
      utils,
      zola-new,
    }:
    utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = import nixpkgs { inherit system; };
      in
      {
        defaultPackage = pkgs.stdenv.mkDerivation {
          name = "bemyak-blog";
          src = ./.;
          buildInputs = [ pkgs.zola ];

          # Must be built with nix build .?submodules=1 --option sandbox false
          # to allow access to kroki
          buildPhase = "zola build";
          installPhase = ''
            mkdir -p $out
            cp -r public $out/
          '';
        };
        devShell = pkgs.mkShell {
          buildInputs = [
            pkgs.zola
            pkgs.woff2
            pkgs.python312Packages.fonttools
            pkgs.nushell
            pkgs.just
            zola-new.outputs.packages.${system}.default
          ];
        };
      }
    );
}
