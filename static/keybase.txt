==================================================================
https://keybase.io/bemyak
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://bemyak.ml
  * I am bemyak (https://keybase.io/bemyak) on keybase.
  * I have a public key ASBh3fP0cRBti6wAUoMkk1aSUlR_bU8IotsmdbyOKYv7-wo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0120b162af5ecddd0274892b06cb7170fff66a3e13db9cf07e1e8722f8c2560aef6e0a",
      "host": "keybase.io",
      "kid": "012061ddf3f471106d8bac0052832493569252547f6d4f08a2db2675bc8e298bfbfb0a",
      "uid": "caae9f2cca22175ffff1d945de3e7719",
      "username": "bemyak"
    },
    "merkle_root": {
      "ctime": 1652164647,
      "hash": "9c71483fef7aa39035c08304883be39ed1fb2b9dc0c8efc899407888d560668377577c35b8d202574ccce60aadcb4535aa127422cb74d11b81a02faca342ac4b",
      "hash_meta": "11ce665484966f175b8ad4aac261414b35a7a2a005692c89bb35d2899045f5c4",
      "seqno": 22455101
    },
    "service": {
      "entropy": "HaKXeX5cAUuNNRCI05A5jhJ0",
      "hostname": "bemyak.ml",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "5.9.3"
  },
  "ctime": 1652164691,
  "expire_in": 504576000,
  "prev": "0883bebcc07234f08a9067c776f82f6962f2ce5cf9a632c6c880bae86d958091",
  "seqno": 12,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgYd3z9HEQbYusAFKDJJNWklJUf21PCKLbJnW8jimL+/sKp3BheWxvYWTESpcCDMQgCIO+vMByNPCKkGfHdvgvaWLyzlz5pjLGyIC66G2VgJHEIAImR3cQYChmsNw6VeSeESLlNvtrqvkqRhew1BA+QDHmAgHCo3NpZ8RAdsIudtDMd8DzYaDRAw3SxyO3VZ/asWUtK8NzoTpeE6F4W9NwQDPNo1Qs66JRdJO/S2ABuTo8lrSKsnWizr8zAahzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIBAFLaOa/zpIfNRH9Bgcck9HjeZmU/QwIIP9nKzGg3+ao3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/bemyak

==================================================================
